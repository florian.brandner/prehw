(*** 
VERSION0 : 
- Pipeline advance handled
- Dependencies handled 
- Post filling handled
- Progress Implemented 
- MISSING : Cache model + Proofs
***)
Require Import Coq.Arith.PeanoNat.
Require Import Lists.List.
Import ListNotations.
Require Import Coq.Arith.Arith.
Set Printing Projections.
Require Import Lia.
Require Import Coq.Bool.Bool.
Require Import Coq.Arith.Compare_dec.

Definition missLat := 3.

Inductive InstrType := 
  | LOAD
  | STORE 
  | ALU
  | BRANCH
  | Empty.

Record Instr := {
  index : nat;
  type : InstrType;
  lat : nat;
  dep : list nat (* dep list size <= 2 *)
  }.

Definition InstrOK (i : Instr) := i.(type) <> Empty -> 1 <= i.(lat).   

Definition StoreOK (i : Instr) := i.(type) = STORE -> 2 <= i.(lat).    

Definition EmptyInstr := Build_Instr 0 Empty 0 nil. 

Definition Prog := list Instr.

Definition ProgOK (p : Prog) := forall i, In i p -> InstrOK i. 

Definition Post := list Instr.
Definition Pre := list Instr.

(******************************************************** Architecture elements *******************************************************************)
(*** PIPELINE ***)
Record Stage := {
  ind : nat;
  inst : Instr;
  baseLat : nat;
  currLat : nat
}.

Check nth.
Definition StageOK (s : Stage)(*(pre : list Instr)*) := s.(currLat) <= s.(baseLat) /\ InstrOK s.(inst). (*InstrOK (nth s.(ind) pre EmptyInstr).*)

Record State := { 
  PRE : list Instr;
  IF : Stage;
  ID : Stage;
  EX : Stage;
  MEM : Stage;
  ST : Stage;
  WB : Stage;
  POST : list Instr
}.

Definition PreOK (s : State)(p : Prog) := forall i, In i s.(PRE) -> InstrOK i. 

(* Definition StateOK (s : State) := (StageOK s.(IF) s.(PRE))  /\ (StageOK s.(ID) s.(PRE)) /\ (StageOK s.(EX) s.(PRE)) 
/\ (StageOK s.(MEM) s.(PRE)) /\ (StageOK s.(WB) s.(PRE)) /\ (StageOK s.(ST) s.(PRE)).*)

Definition StateOK (s : State) := StageOK s.(IF)  /\ StageOK s.(ID) /\ StageOK s.(EX)
/\ StageOK s.(MEM) /\ StageOK s.(WB) /\ StageOK s.(ST).

(* Print State. *)

Definition PS := Build_Stage 0 (Build_Instr 0 LOAD 2 [])  3 1.

Definition EmptyPS := Build_Stage 0 EmptyInstr 1 1. (* Empty Pipeline Stage *)

Definition ErrorPS := Build_Stage 0 EmptyInstr 0 1. (* Error Pipeline Stage : used in proving valid state*)

Definition EmptyPipe := Build_State [] EmptyPS EmptyPS EmptyPS EmptyPS EmptyPS EmptyPS [].

(** Cache **)
Definition DChit := list bool.
Definition IChit := list bool.

(******************************************************** Comparison functions ********************************************************************)
Fixpoint ListEq (l1 l2 : list nat) : bool :=
  match l1 with
  | nil => 
    match l2 with 
    | nil => true
    | h2::t2 => false
    end
  | h1::t1 => 
    match l2 with 
      | nil => false
      | h2::t2 =>
        if h1 =? h2 then ListEq t1 t2
        else false
    end
  end.

Definition InstrEq (i1 i2 : Instr) : bool :=
  if i1.(index) =? i2.(index) then
    if i1.(lat) =? i2.(lat) then
      if (ListEq i1.(dep) i2.(dep)) then 
        match i1.(type), i2.(type) with
        | LOAD, LOAD => true
        | STORE, STORE => true
        | ALU, ALU => true
        | BRANCH, BRANCH => true
        | Empty, Empty => true
        | _, _ => false
        end
      else false
    else false
  else false.

Definition StageEq (s1 s2 : Stage) : bool :=
  if s1.(ind) =? s2.(ind) then 
    if s1.(baseLat) =? s2.(baseLat) then 
      if s1.(currLat) =? s2.(currLat) then 
        if InstrEq s1.(inst) s2.(inst) then true
        else false
      else false
    else false
  else false.

Fixpoint PreEq (l1 l2 : list Instr) : bool :=
  match l1 with
  | nil => 
    match l2 with 
    | nil => true
    | h2::t2 => false
    end
  | h1::t1 => 
    match l2 with 
      | nil => false
      | h2::t2 =>
        if InstrEq h1 h2 then PreEq t1 t2
        else false
    end
  end.

(******************************************************** Stages Control Variables ********************************************************************)
Definition NxtWBfree (s : State) : bool := true. (* WB stage has a latency of 1*) (*DONE*)

Definition NxtSTfree (s : State) : bool := (*DONE*)
  if StageEq s.(ST) EmptyPS  then true
  else
    if leb s.(ST).(currLat) s.(ST).(baseLat) then
      if s.(ST).(baseLat) =? s.(ST).(currLat) then true
      else false
    else false. (* ERROR case *)

Definition NxtMEMfree (s : State) : bool := (*DONE*)
  if StageEq s.(MEM) EmptyPS then true
  else
    match s.(MEM).(inst).(type) with
    | STORE => if NxtSTfree s then true else false
    | _ => 
      (* Case where MEM =/ Empty and not store*)
      if leb s.(MEM).(currLat) s.(MEM).(baseLat) then
        if s.(MEM).(baseLat) =? s.(MEM).(currLat) then true
        else false
      else false (* ERROR case *)
     end.

Definition StPending (s : State) : bool :=
  if StageEq s.(MEM) EmptyPS then 
    if NxtSTfree s then false
    else true
  else
    match s.(MEM).(inst).(type) with
      | STORE => true
      | _ => if NxtSTfree s then false else true
    end.

Definition NxtEXfree (s : State)(Dchit : DChit) : bool :=  (* depends on instr latency *)
  if StageEq s.(EX) EmptyPS then true
  else
    if negb (StPending s) then
      if leb s.(EX).(currLat) s.(EX).(baseLat) then
        if s.(EX).(baseLat) =? s.(EX).(currLat) then if NxtMEMfree s then true else false
        else false
      else false (* ERROR case *)
    else if (nth s.(EX).(ind) Dchit false) then 
      if leb s.(EX).(currLat) s.(EX).(baseLat) then
        if s.(EX).(baseLat) =? s.(EX).(currLat) then if NxtMEMfree s then true else false
        else false
      else false
    else 
      match s.(EX).(inst).(type) with
      | STORE | LOAD => false
      | _ => if leb s.(EX).(currLat) s.(EX).(baseLat) then
          if s.(EX).(baseLat) =? s.(EX).(currLat) then if NxtMEMfree s then true else false
          else false
        else false 
      end.

Definition NxtIDfree (s : State) (d : DChit) : bool := 
  if StageEq s.(ID) EmptyPS then true
  else 
    if NxtEXfree s d then 
      if (ListEq s.(ID).(inst).(dep) nil) then true
      else (* There are dependencies => check if they are done *)
        
        (* Case of 1 dependency *)
        if (length s.(ID).(inst).(dep)) =? 1 then 
          if s.(EX).(inst).(index) =? (nth 0 s.(ID).(inst).(dep) 0) then false (* dependency still unsolved *)
          else if s.(MEM).(inst).(index) =? (nth 0 s.(ID).(inst).(dep) 0) then 
            if s.(MEM).(baseLat) =? s.(MEM).(currLat) then true (* dependency output ready *)
            else false
          else true (* It passed EX/MEM => ready *)

        (* Case of 2 dependencies *)
        else (* if 1 out of 2 is missing => false, if 1 is valid check 2 *)
          if s.(EX).(inst).(index) =? (nth 0 s.(ID).(inst).(dep) 0) then false 
          else if s.(MEM).(inst).(index) =? (nth 0 s.(ID).(inst).(dep) 0) then 
            if s.(MEM).(baseLat) =? s.(MEM).(currLat) then 

              if s.(EX).(inst).(index) =? (nth 1 s.(ID).(inst).(dep) 0) then false (* it cannot be at MEM since dep0 is *)
              else true 

            else false

          else (* dep0 checked *)
            if s.(EX).(inst).(index) =? (nth 1 s.(ID).(inst).(dep) 0) then false 
            else if s.(MEM).(inst).(index) =? (nth 1 s.(ID).(inst).(dep) 0) then 
              if s.(MEM).(baseLat) =? s.(MEM).(currLat) then true (* dependency output ready *)
              else false
            else true 

    else false. 

Definition NxtIFfree (s : State) (d : DChit) : bool :=  (* depends on cache hit=1 / miss=lat *)
  if StageEq s.(IF) EmptyPS then true  
  else
    if leb s.(IF).(currLat) s.(IF).(baseLat) then
      if s.(IF).(baseLat) =? s.(IF).(currLat) then if NxtIDfree s d then true else false
      else false
    else false. (* ERROR case *)


(* RECHECK OR CASES PROPERLY FOR BR+MEM PENDING *)
Definition MemPending (s : State) : bool := 
  if StageEq s.(IF) EmptyPS then 
    if StageEq s.(ID) EmptyPS then 
      if StageEq s.(EX) EmptyPS then
        if NxtMEMfree s then (if StPending s then true else false)
        else true

      else (* EX not empty *)
        match s.(EX).(inst).(type) with
      | STORE | LOAD => true
      | _ => 
        if NxtMEMfree s then (if StPending s then true else false)
        else true
      end

    else (* ID not empty *)
      match s.(ID).(inst).(type) with
      | STORE | LOAD => true
      | _ => 
          if StageEq s.(EX) EmptyPS then
          if NxtMEMfree s then (if StPending s then true else false)
          else true
        else (* EX not empty *)
          match s.(EX).(inst).(type) with
            | STORE | LOAD => true
            | _ => 
              if NxtMEMfree s then (if StPending s then true else false)
              else true
            end
    end

  else
    match s.(IF).(inst).(type) with
      | STORE | LOAD => true
      | _ => 
        if StageEq s.(ID) EmptyPS then 
          if StageEq s.(EX) EmptyPS then
            if NxtMEMfree s then (if StPending s then true else false)
            else true
          else (* EX not empty *)
            match s.(EX).(inst).(type) with
          | STORE | LOAD => true
          | _ => 
            if NxtMEMfree s then (if StPending s then true else false)
            else true
          end
      else (* ID not empty *)
        match s.(ID).(inst).(type) with
        | STORE | LOAD => true
        | _ => 
            if StageEq s.(EX) EmptyPS then
            if NxtMEMfree s then (if StPending s then true else false)
            else true
          else (* EX not empty *)
            match s.(EX).(inst).(type) with
              | STORE | LOAD => true
              | _ => 
                if NxtMEMfree s then (if StPending s then true else false)
                else true
              end
        end
    end.

Definition BrPending (s : State)(d : DChit) : bool :=
  if StageEq s.(IF) EmptyPS then 
    if StageEq s.(ID) EmptyPS then 
      if NxtEXfree s d then
        if StageEq s.(EX) EmptyPS then false

        else (* EX not empty *)
          if leb s.(EX).(currLat) s.(EX).(baseLat) then
            if s.(EX).(baseLat) =? s.(EX).(currLat) then false
            else 
              match s.(EX).(inst).(type) with
              | BRANCH => true
              | _ => false
            end
          else false

      else true 

    else (* ID not empty *)
      match s.(ID).(inst).(type) with
      | BRANCH => true
      | _ => 
        if NxtEXfree s d then
          if StageEq s.(EX) EmptyPS then false
          else (* EX not empty *)
            if leb s.(EX).(currLat) s.(EX).(baseLat) then
              if s.(EX).(baseLat) =? s.(EX).(currLat) then false
              else 
                match s.(EX).(inst).(type) with
                | BRANCH => true
                | _ => false
              end
            else false
        else true 
    end
  
  else (* IF not empty *)
    match s.(IF).(inst).(type) with
      | BRANCH => true
      | _ =>    
        if StageEq s.(ID) EmptyPS then 
          if NxtEXfree s d then
            if StageEq s.(EX) EmptyPS then false
            else 
              if leb s.(EX).(currLat) s.(EX).(baseLat) then
                if s.(EX).(baseLat) =? s.(EX).(currLat) then false
                else 
                  match s.(EX).(inst).(type) with
                  | BRANCH => true
                  | _ => false
                end
              else false
          else true
        else 
          match s.(ID).(inst).(type) with
          | BRANCH => true
          | _ => false
        end
    end.

Definition ReadyIF (s : State) (cnt : nat)(Ichit : IChit) (d : DChit) : bool :=
  if BrPending s d then false
  else
    if negb (MemPending s) then true (* Ichit[Instr(pc).ind] \/ ~MemPending *)
    else 
      if (nth (cnt-1) Ichit false) then true
      else false.

Compute (nth 2 [true; false] false).

(******************************************************** Pipeline stages update **********************************************************)
Definition UpdateStage (s : Stage) : Stage :=
  Build_Stage s.(ind) s.(inst) s.(baseLat) (s.(currLat)+1).

Definition Prog_Pc_Instr (pc : nat) (p : Prog) := nth pc p EmptyInstr. 

Definition UpdatePRE (s : State)(pc : nat)(p : Prog) : list Instr :=
  if (pc <? length(p)) then s.(PRE) ++ [Prog_Pc_Instr pc p]
  else s.(PRE).

Definition UpdateCnt (s : State) (cnt : nat) : nat :=
  if InstrEq s.(IF).(inst) EmptyInstr then 
    if length s.(PRE) =? 0 then 1
    else cnt
  else s.(IF).(ind)+1.

Definition UpdateIF (s : State) (cnt : nat) (pc : nat) (p : Prog) (i : IChit) (d: DChit) : Stage := 
  if (pc <=? length(p)) then 
    if NxtIFfree s d then
      if ReadyIF s cnt i d then (* New Instr *)
        if 1 <=? cnt then
          if cnt <=? length s.(PRE) then
            Build_Stage cnt (nth (cnt-1) s.(PRE) EmptyInstr) 1 1 (* Split cases of Ichit/miss *)
          else s.(IF)
        else s.(IF)
      else EmptyPS
    else 
       if leb s.(IF).(currLat) s.(IF).(baseLat) then
         if s.(IF).(baseLat) =? s.(IF).(currLat) then s.(IF)
         else UpdateStage s.(IF)
       else ErrorPS
  else ErrorPS.

Definition Updatepc (s : State) (cnt : nat) (pc : nat) (p : Prog)(i : IChit) (d: DChit) : nat :=
  if (length(p) <=? pc) then pc
  else 
    if StageEq (UpdateIF s cnt pc p i d) ErrorPS then pc
    else if StageEq (UpdateIF s cnt pc p i d) EmptyPS then pc
    else if StageEq (UpdateIF s cnt pc p i d) s.(IF) then pc
    else pc+1.

Definition UpdateID (s : State) (d: DChit) : Stage :=  (*TO BE FIXED *)
  if NxtIDfree s d then
    if NxtIFfree s d then (* ID does not need latency info or maybe doesn't hurt to have it*)
    Build_Stage s.(IF).(ind) s.(IF).(inst) s.(IF).(baseLat) s.(IF).(baseLat)
    else EmptyPS
  else 
    s.(ID).

Definition UpdateEX (s : State)(d: DChit) : Stage :=
  if NxtEXfree s d then
    if StageEq s.(ID) EmptyPS then EmptyPS
    else if NxtIDfree s d then 
      match s.(ID).(inst).(type) with
      | STORE | LOAD => Build_Stage s.(ID).(ind) s.(ID).(inst) 1 1
      | Empty => Build_Stage s.(ID).(ind) s.(ID).(inst) 1 1
      | _ => Build_Stage s.(ID).(ind) s.(ID).(inst) s.(ID).(inst).(lat) 1
      end
    else EmptyPS
  else
    if leb s.(EX).(currLat) s.(EX).(baseLat) then
     if s.(EX).(baseLat) =? s.(EX).(currLat) then s.(EX)
     else UpdateStage s.(EX)
    else ErrorPS.

Definition UpdateMEM (s : State)(d: DChit) : Stage := 
  if NxtMEMfree s then
    if StageEq s.(EX) EmptyPS then EmptyPS
    else if NxtEXfree s d then 
      match s.(EX).(inst).(type) with
      | LOAD => Build_Stage s.(EX).(ind) s.(EX).(inst) s.(EX).(inst).(lat) 1 (* INCOMPLETE : MUST HANDLE DCACHE MISS *)
      | _ => Build_Stage s.(EX).(ind) s.(EX).(inst) 1 1 (* STORE also included here since bus access starts here*) 
      end
    else EmptyPS
  else 
    if leb s.(MEM).(currLat) s.(MEM).(baseLat) then
      if s.(MEM).(baseLat) =? s.(MEM).(currLat) then s.(MEM)
      else UpdateStage s.(MEM)
    else ErrorPS.

Definition UpdateST (s : State) : Stage := 
  if NxtSTfree s then 
    match s.(MEM).(inst).(type) with
    | STORE => Build_Stage s.(MEM).(ind) s.(MEM).(inst) (s.(MEM).(inst).(lat)-1) 1 (* Store latency > 1 *)
    | _ => 
      if negb (NxtMEMfree s) then EmptyPS
      else if StageEq s.(MEM) EmptyPS then EmptyPS
      else s.(ST)
    end
  else
  if leb s.(ST).(currLat) s.(ST).(baseLat) then
      if s.(ST).(baseLat) =? s.(ST).(currLat) then EmptyPS
      else UpdateStage s.(ST)
   else ErrorPS.

Definition UpdateWB (s : State) : Stage := 
  if NxtMEMfree s then 
    match s.(MEM).(inst).(type) with
    | STORE | Empty => EmptyPS
    | _ =>Build_Stage s.(MEM).(ind) s.(MEM).(inst) 1 1
    end
  else EmptyPS.

Fixpoint appendProg (l1 l2 : list Instr) : list Instr :=
  match l1 with
  | nil => l2
  | cons i l1' => cons i (appendProg l1' l2)
  end.

Definition UpdatePOST (s : State): list Instr := (* To be used for proving end of program execution *)
  if NxtSTfree s then
    if StageEq s.(ST) EmptyPS then 
      if NxtWBfree s then 
        if StageEq s.(WB) EmptyPS then s.(POST)
        else appendProg s.(POST) [ s.(WB).(inst) ]
      else s.(POST)
    else appendProg s.(POST) [ s.(ST).(inst) ]
  else 
    if NxtWBfree s then 
      if StageEq s.(WB) EmptyPS then s.(POST)
      else appendProg s.(POST) [ s.(WB).(inst) ]
    else s.(POST).

(******************************************************** FULL PIPELINE UPDATE **********************************************************)
(* Backward update since a stage only needs info of previous stages *)
Definition Update (s : State) (cnt : nat) (pc : nat) (p : Prog)(d: DChit)(i : IChit) : State := 
  Build_State (UpdatePRE s pc p) (UpdateIF s cnt pc p i d) (UpdateID s d) (UpdateEX s d) (UpdateMEM s d) (UpdateST s) (UpdateWB s) (UpdatePOST s).

(******************************************************** RUN FUNCTIONS **********************************************************)
Fixpoint Exec (s : State) (cycles : nat) (cnt : nat) (p : Prog) (pc : nat) (i : IChit) (d: DChit) : State :=
  match cycles with
  | O => s
  | S cycles' => 
    let cnt' := UpdateCnt s cnt in 
    let s' := Update s cnt' pc p d i in
    Exec s' cycles' (UpdateCnt s' cnt') p (Updatepc s' (UpdateCnt s' cnt') pc p d i) i d
(* Exec (Update s (UpdateCnt s cnt) pc p d i) cycles' (UpdateCnt (Update s (UpdateCnt s cnt) pc p d i) cnt) p (Updatepc s (UpdateCnt s cnt) pc p d i) i d (* Update pc problem to be fixed *) *)
  end.

(*Fixpoint FillPost (s : State) (cycles : nat) (p : Prog) (pc : nat) (i : IChit) (d: DChit) : Post :=
  match cycles with
  | O => s.(POST)
  | S cycles' =>  FillPost (Update s pc p i d) cycles' p (Updatepc s pc p i d) (UpdatePOST s)
  end.
*)

(******************************************************** TESTS **********************************************************)
Definition P := [(Build_Instr 1 ALU 3 nil); (Build_Instr 2 LOAD 2 nil); (Build_Instr 3 STORE 2 nil) ].
Compute length P.

Definition d := [ true; true; true].
Definition i := [ true; true; false].

Definition cnt0 := UpdateCnt EmptyPipe 0.
Compute cnt0.
Definition pc0 := 0.
Definition S1 := Update EmptyPipe cnt0 pc0 P d i. 
Compute S1.
Definition cnt1 := UpdateCnt S1 cnt0.
Compute cnt1.
Definition pc1 := Updatepc S1 cnt1 0 P d i.
Definition S2 := Update S1 cnt1 pc1 P d i.
Compute S2.
Definition cnt2 := UpdateCnt S2 cnt1.
Compute cnt2.
Definition pc2 := Updatepc S2 cnt2 pc1 P d i.
Definition S3 := Update S2 cnt2 pc2 P d i.
Compute S3.
Definition cnt3 := UpdateCnt S3 cnt2.
Compute cnt3.
Compute Updatepc S3 cnt3 2 P d i.
Definition S4 := Update S3 cnt3 3 P d i.
Compute S4.
Definition cnt4 := UpdateCnt S4 cnt3.
Compute cnt4.
Compute Updatepc S4 cnt4 3 P d i.
Definition S5 := Update S4 cnt4 3 P d i.
Compute S5.
Definition cnt5 := UpdateCnt S5 cnt4.
Compute cnt5.
Definition S6 := Update S5 cnt5 3 P d i.
Compute S6.
Definition cnt6 := UpdateCnt S6 cnt5.
Compute cnt6.
Definition S7 := Update S6 cnt6 3 P d i.
Compute S7.
Definition cnt7 := UpdateCnt S7 cnt6.
Compute cnt7.
Definition S8 := Update S7 cnt7 3 P d i.
Compute S8. 
Definition cnt8 := UpdateCnt S8 cnt7.
Compute cnt8.
Definition S9 := Update S8 cnt8 3 P d i.
Compute S9.
Definition cnt9 := UpdateCnt S9 cnt8.
Compute cnt9.   
Definition S10 := Update S9 cnt9 3 P d i.
Compute S10.
Definition cnt10 := UpdateCnt S10 cnt9.
Compute cnt10.
Definition S11 := Update S10 cnt10 3 P d i. 
Compute S11.
Definition cnt11 := UpdateCnt S11 cnt10.
Compute cnt11.
Definition S12 := Update S11 cnt11 3 P d i.
Compute S12.
Definition cnt12 := UpdateCnt S12 cnt11.
Definition S13 := Update S12 cnt12 3 P d i.
Compute S13.
Definition cnt13 := UpdateCnt S13 cnt12.
Definition S14 := Update S13 cnt13 3 P d i.
Compute S14.
Definition cnt14 := UpdateCnt S14 cnt13.
Compute cnt14.
Definition S15 := Update S14 cnt14 3 P d i.
Compute S15.

Definition test := Exec EmptyPipe 15 0 P 0 i d.
Compute test.
(* Compute FillPost EmptyPipe 14 P 0 post. *)

(*** DEPENDENCIES test ***)
Definition Pg2 := [(Build_Instr 1 ALU 2 nil); (Build_Instr 2 ALU 3 [1]); (Build_Instr 3 STORE 2 nil) ].
Compute Pg2.

Compute Exec EmptyPipe 7 0 Pg2 0 i d.
(* Compute FillPost EmptyPipe 14 Pg2 0 post. *)

(******************************************************** PROOFS **********************************************************)
(*** For all programs, exists a number of cycles for which it finishes execution ***)
Lemma Finish : forall (p : Prog)(i : IChit)(d : DChit), exists (c : nat), (Exec EmptyPipe c 0 p 0 i d).(POST) = p. 
Proof.
intros.
unfold Exec. 
Admitted.

Lemma EqS1S2 : forall (s1 s2: Stage), 
s1 = s2 -> s1.(inst) = s2.(inst) /\ s1.(baseLat) = s2.(baseLat) /\ s1.(currLat) = s2.(currLat).
Proof.
intros. rewrite H. split. split. split. reflexivity. reflexivity. Qed.


Lemma NotZero : forall (a b : nat), a < b -> b <> 0.
intros.
induction b.
exfalso.  apply Nat.nlt_0_r in H. exact H.
apply Nat.neq_succ_0.
Qed.

Lemma NotErrorPS : forall (s : Stage), s.(baseLat) = s.(currLat) -> s <> ErrorPS.
Proof.
intros. unfold not. intros. unfold ErrorPS in H0.   
destruct s. simpl in H. inversion H0. rewrite H in H4. rewrite H4 in H5. discriminate.
Qed.

(*** Valid stage -> Update = Valid stage ***) 
Lemma ValidIF (s : State)(cnt : nat)(pc : nat)(p : Prog)(i : IChit)(d : DChit) : 
StageOK s.(IF) -> pc <= length p -> PreOK s p -> StageOK (UpdateIF s cnt pc p i d).
Proof.
intros.
unfold UpdateIF.
apply Nat.leb_le in H0. rewrite H0.
unfold StageOK in H. destruct H as [Hlat Hinst].
assert (Hlatp := Hlat). apply Nat.leb_le in Hlat. rewrite Hlat. unfold StageOK. split. 
  * destruct (NxtIFfree s).
    + destruct (ReadyIF s).
      - destruct (cnt <=? length s.(PRE)) eqn:Hcnt.
        { simpl. destruct (cnt) eqn:Hcnt2. 
          * exact Hlatp.
          * reflexivity. }
        { simpl. destruct (cnt) eqn:Hcnt2; try (exact Hlatp). }   
       - reflexivity.
    + destruct (s.(IF).(baseLat) =? s.(IF).(currLat)) eqn:Heq.
      - apply Hlatp.
      - unfold UpdateStage. simpl. apply Nat.eqb_neq in Heq. lia.
  * destruct (NxtIFfree s).
    + destruct (ReadyIF s).
      - destruct (cnt <=? length s.(PRE)) eqn:Hcnt.
        { simpl. destruct (cnt) eqn:Hcnt2. 
          * exact Hinst.
          * simpl. unfold PreOK in H1. apply H1. 
Admitted.

 (*unfold InstrOK. intros.

 unfold InstrOK. simpl. intros. reflexivity. }
        { apply Hlatp. }   
       - reflexivity.
    + destruct (s.(IF).(baseLat) =? s.(IF).(currLat)) eqn:Heq.
      - apply Hlatp.
      - unfold UpdateStage. simpl. apply Nat.eqb_neq in Heq. lia.

 unfold StageOK, EmptyPS. split.
      - reflexivity.
      - simpl. unfold InstrOK, EmptyInstr. simpl. contradiction.
  * assert(Hstage:=H). destruct H as [Hlat Hinst]. apply Nat.leb_le in Hlat as Hlat2. rewrite Hlat2.
    destruct (baseLat (IF s) =? currLat (IF s)) eqn:Heq.
    - exact Hstage.
    - unfold UpdateStage. unfold StageOK. simpl.
      assert (H3: (currLat (IF s) < baseLat (IF s))).
      { apply Nat.le_neq. split.
        * apply Hlat.
        * apply EqNat.beq_nat_false_stt in Heq as Hneq. symmetry. exact Hneq. }
      rewrite <- Nat.le_succ_l in H3. 
      rewrite Nat.add_1_r. split.
        + exact H3.
        + exact Hinst.
Qed.*)

Lemma ValidID (s : State)(d : DChit) : 
StateOK s -> StageOK (UpdateID s d).
Proof.
intros.
unfold UpdateID.
destruct (NxtIDfree s).
  - destruct (NxtIFfree s). 
    * unfold StageOK. simpl. split. 
     + reflexivity.
     + unfold StateOK in H. destruct H. apply H. 
    * unfold EmptyPS, StageOK. simpl. split. 
     + reflexivity.
     + unfold EmptyInstr, InstrOK. simpl. contradiction. 
  - apply H.
Qed.

Lemma ValidEmpty : InstrOK EmptyInstr.
Proof.
unfold InstrOK, EmptyInstr. contradiction. Qed.

Lemma EmptyPSOK : StageOK EmptyPS.
Proof.
unfold StageOK, EmptyPS. split. 
  * reflexivity.
  * simpl. apply ValidEmpty.
Qed.

Hint Resolve ValidEmpty EmptyPSOK : core.

Lemma ValidEX (s : State) (d : DChit):
StateOK s -> StageOK (UpdateEX s d).
Proof.
intros.
unfold UpdateEX.
destruct (NxtEXfree s).
  - destruct (StageEq (ID s) EmptyPS) eqn:H1.
    * unfold StageOK, EmptyPS. split; auto.
    * destruct (NxtIDfree s).
      + destruct (type (inst (ID s))) eqn:H2; unfold StageOK; simpl; split; try (reflexivity); apply H; rewrite H2; discriminate.
      + auto.
  - unfold StateOK in H.  destruct H as [_ [_ [H1 _]]]. assert (Hstage:=H1). destruct H1 as [Hl H2]. apply Nat.leb_le in Hl as Hl2. rewrite Hl2.
     destruct (s.(EX).(baseLat) =? s.(EX).(currLat)) eqn:H3. 
     + exact Hstage.
     + unfold UpdateStage, StageOK. simpl. split.
       * apply Nat.eqb_neq in H3. lia.
       * exact H2.
Qed.
        

Lemma ValidMEM (s : State)(d : DChit):
StateOK s -> StageOK (UpdateMEM s d). 
Proof.
intros. unfold UpdateMEM. 
destruct (NxtMEMfree s).
  * destruct (StageEq s.(EX) EmptyPS) eqn:Hemp.
    + unfold StageOK. simpl. split. 
      { reflexivity. }
      { auto. } 
    + destruct (NxtEXfree s d).
      { destruct (s.(EX).(inst).(type)) eqn:Htype; unfold StageOK; simpl; split; try (apply H); try (reflexivity).
      rewrite Htype. unfold not. intros. discriminate. }
      { apply EmptyPSOK. }
  * unfold StateOK in H. destruct H as [H1 [H2 [H3 [H4 H5]]]]. assert (Hmem:=H4). 
    unfold StageOK in Hmem. destruct Hmem.
    assert (Hm:= H). apply Nat.leb_le in H. rewrite H. 
    destruct (s.(MEM).(baseLat) =? s.(MEM).(currLat)) eqn:Heq.
    + apply H4.
    + unfold UpdateStage. unfold StageOK. simpl. split. 
      { apply EqNat.beq_nat_false_stt in Heq. lia. }
      { apply H0. }
Qed. 

Lemma ValidWB (s : State): 
StateOK s -> StageOK (UpdateWB s). 
Proof.
intros.
unfold UpdateWB.
destruct (NxtMEMfree s).
  * destruct (s.(MEM).(inst).(type)) eqn:Htype; try (apply EmptyPSOK); try (unfold StageOK; simpl; split); try (reflexivity); try (apply H).
  * apply EmptyPSOK.
Qed.

Lemma ValidST (s : State) : 
StateOK s -> StoreOK s.(MEM).(inst) -> StageOK (UpdateST s). 
Proof.
intros.
unfold UpdateST.
unfold StateOK in H; destruct H as [H1 [H2 [H3 [H4 [H6 H7]]]]]; 
assert (Hmem:=H4); unfold StageOK in Hmem; destruct Hmem; try (apply H6). 
destruct (NxtSTfree s).
  * destruct (type (inst (MEM s))) eqn:Htype.
    + destruct (negb (NxtMEMfree s)).
      { apply EmptyPSOK. }
      { destruct (StageEq (MEM s) EmptyPS) eqn:Hstage; try (apply EmptyPSOK); try (apply H7). }
    + unfold StageOK. simpl. split.
      - assert (Hlat : 1 <= s.(MEM).(inst).(lat) - 1 <-> 2 <= s.(MEM).(inst).(lat)). { lia. }
        rewrite Hlat. apply H0. exact Htype.
      - exact H5.
    + destruct (NxtMEMfree s); simpl; try (apply EmptyPSOK).
      { destruct (StageEq s.(MEM) EmptyPS) eqn:Hemp; try (apply EmptyPSOK); try (apply H7). }
    + destruct (NxtMEMfree s); simpl; try (apply EmptyPSOK).
      { destruct (StageEq s.(MEM) EmptyPS) eqn:Hemp; try (apply EmptyPSOK); try (apply H7). }
    + destruct (NxtMEMfree s); simpl; try (apply EmptyPSOK).
      { destruct (StageEq s.(MEM) EmptyPS) eqn:Hemp; try (apply EmptyPSOK); try (apply H7). }
  * assert (Hstore := H7). unfold StageOK in H7; destruct H7. assert (Hneq:=H7). apply Nat.leb_le in H7; rewrite H7.
    destruct(s.(ST).(baseLat) =? s.(ST).(currLat)) eqn:Heq.
    + apply EmptyPSOK.
    + unfold UpdateStage; unfold StageOK; simpl; split.
      { apply Nat.eqb_neq in Heq. lia. }
      exact H8.
Qed. 

Lemma ValidState : forall (s : State) (cnt : nat) (pc : nat) (p : Prog)(d: DChit)(i : IChit) ,
StateOK s -> pc <= length p -> PreOK s p -> StoreOK s.(MEM).(inst) -> StateOK (Update s cnt pc p d i).
Proof.
intros s cnt pc p d i H Hpc Hpre Hstore.
assert (Hstate:=H). unfold StateOK in H; destruct H as [H1 [H2 [H3 [H4 [H6 H7]]]]].
unfold Update; unfold StateOK; simpl; repeat split.
  * apply ValidIF. exact H1. exact Hpc. exact Hpre.
  * apply ValidIF. exact H1. exact Hpc. exact Hpre.
  * apply ValidID; exact Hstate.
  * apply ValidID. exact Hstate.
  * apply ValidEX. exact Hstate.
  * apply ValidEX. exact Hstate.
  * apply ValidMEM. exact Hstate.
  * apply ValidMEM. exact Hstate.
  * apply ValidWB. exact Hstate.
  * apply ValidWB. exact Hstate.
  * apply ValidST. exact Hstate. apply Hstore.
  * apply ValidST. exact Hstate. apply Hstore.
Qed.

(****************************************************** VERSION1 ******************************************************************)

Inductive Progress : Type :=
  | Same : Progress
  | ST1 : Progress
  | ST2 : Progress.

Definition InstrStage (i : nat) (s :State) : nat * nat :=
  if s.(IF).(ind) =? i then (1, (s.(IF).(baseLat) - s.(IF).(currLat)))
  else if s.(ID).(ind) =? i then (2, (s.(ID).(baseLat) - s.(ID).(currLat)))
  else if s.(EX).(ind) =? i then (3, (s.(EX).(baseLat) - s.(EX).(currLat)))
  else if s.(MEM).(ind) =? i then (4, (s.(MEM).(baseLat) - s.(MEM).(currLat)))
  else if s.(ST).(ind) =? i then (5, (s.(ST).(baseLat) - s.(ST).(currLat)))
  else if s.(WB).(ind) =? i then (5, (s.(WB).(baseLat) - s.(WB).(currLat)))
  else 
    if i <=? length s.(PRE) then (0, 0)
    else (6, 0).

Definition InstrProgress (i : nat) (s1 s2 : State) : Progress :=
  let (stage1, n1) := InstrStage i s1 in  
  let (stage2, n2) := InstrStage i s2 in
    match Nat.compare stage1 stage2 with
    | Eq => match Nat.compare n1 n2 with
            | Eq => Same
            | Gt => ST1
            | Lt => ST2
            end
    | Gt => ST1
    | Lt => ST2
    end.

Lemma SSSI : forall (i : nat) (s1 s2 : State), s1 = s2 -> InstrProgress i s1 s2 = Same.
Proof.
  intros.
  unfold InstrProgress.
  rewrite H. 
  Admitted.
  
Compute S9.
Compute InstrStage 1 S1.
Compute InstrStage 1 S2.
Compute InstrProgress 1 S1 S2.

From Coq Require Import Arith Lia Program.
From Equations Require Import Equations.

Fixpoint StateProgress (l1 l2 : nat)(s1 s2 : State): Progress := 
  if l1 <=? l2 then
  match l1, l2 with
    | O, O | _, O => Same
    | O, _ => ST2
    | S l1', S l2' =>  match InstrProgress (l1'+1) s1 s2 with
               | Same => StateProgress l1' l2' s1 s2 
               | ST1 => ST1
               | ST2 => ST2
               end
    end
  else
  match l1, l2 with
    | O, O | O, _ => Same
    | _, 0 => ST1
    | S l1', S l2' =>  match InstrProgress (l2'+1) s1 s2 with
               | Same => StateProgress l1' l2' s1 s2 
               | ST1 => ST1
               | ST2 => ST2
               end
    end.

Compute StateProgress (length S2.(PRE)) (length S4.(PRE)) S2 S4.
Compute StateProgress (length S5.(PRE)) (length S7.(PRE)) S5 S7.
Compute StateProgress (length S3.(PRE)) (length S3.(PRE)) S3 S3.


(***)

From Ltac2 Require Import Ltac2 Constr.

Ltac2 reveal_fixpoint fn :=
  match Unsafe.kind fn with
  | Unsafe.Constant cst _ =>
    Std.eval_unfold [(Std.ConstRef cst, Std.AllOccurrences)] fn
  | _ => Control.throw Not_found
  end.

Ltac2 unfold_fixpoint_once fn :=
  let fixpoint := reveal_fixpoint fn in
  match Unsafe.kind fixpoint with
  | Unsafe.Fix _ _ binders bodies =>
    let binder := Array.get binders 0 in
    let unbound_body := Array.get bodies 0 in
    let body := Unsafe.substnl [fn] 0 unbound_body in
    match Unsafe.check body with
    | Val b => b
    | Err error => Control.throw error
    end
  | _ => Control.throw Not_found
  end.

Notation beta x :=
  ltac:(let x := (eval cbv beta in x) in exact x) (only parsing).

Definition Unfold_SP := forall (s1 s2 : State),
StateProgress (length s1.(PRE)) (length s2.(PRE)) s1 s2 =
beta (ltac2: (let fn := unfold_fixpoint_once constr:(StateProgress) in exact $fn) (length s1.(PRE)) (length s2.(PRE)) s1 s2).

(***)

Axiom EmptyState : forall (s : State), length s.(PRE) = 0 <-> s = EmptyPipe.
(* Definition NotEmpty (cnt : nat) (pc : nat)(d: DChit)(i : IChit)(s : State)(p : Prog) := s = EmptyPipe ->  Update s cnt pc p d i <> EmptyPipe.  *)
Axiom NotEmpty : forall (cnt : nat) (pc : nat)(d: DChit)(i : IChit)(s : State)(p : Prog), s = EmptyPipe ->  Update s cnt pc p d i <> EmptyPipe. 

Lemma EqProgress : forall (cnt : nat) (pc : nat) (p : Prog)(d: DChit)(i : IChit)(s: State), 
StateProgress (length s.(PRE)) (length s.(PRE)) s s = Same.
Proof.
  intros.
  unfold StateProgress.
  Abort.


(*** Lemma1: SIC paper ***) 
(*** forall (s1 s2 : State), StateProgress l1 l2 s1 s2 = Same -> Update s1 cnt pc p d i = Update s2 cnt pc p d i.
PS : the condition j <= i is already included in StateProgress in the selection based on PRE length ***)
Lemma ProgressDependence : forall (cnt : nat) (pc : nat) (p : Prog)(d: DChit)(i : IChit)(s1 s2 : State),
beta (ltac2: (let fn := unfold_fixpoint_once constr:(StateProgress) in exact $fn) (length s1.(PRE)) (length s2.(PRE)) s1 s2) = Same -> 
Update s1 cnt pc p d i = Update s2 cnt pc p d i.
Proof.
intros.
f_equal.
destruct (length s1.(PRE) <=? length s2.(PRE)) eqn:Heq.
  * destruct (length s1.(PRE)) eqn:H1.
    + destruct (length s2.(PRE)) eqn:H2.
      - assert (Hs1 : s1 = EmptyPipe). { apply EmptyState in H1. exact H1. }
        assert (Hs2 : s2 = EmptyPipe). { apply EmptyState in H2. exact H2. }
        rewrite Hs1, Hs2. reflexivity.
      - discriminate.
    + destruct (length s2.(PRE)) eqn:H2. 
      - discriminate.
      - unfold InstrProgress in H; simpl in *.
Admitted.


(*** Lemma2: SIC paper ***) 
Lemma PositiveProgress : forall (cnt : nat) (pc : nat)(d: DChit)(i : IChit)(s : State)(p : Prog), 
let s' := Update s cnt pc p d i in
let l := length s.(PRE) in 
let l' := length s'.(PRE) in
beta (ltac2: (let fn := unfold_fixpoint_once constr:(StateProgress) in exact $fn) l l' s s') = ST2.
Proof.
intros.
destruct (l <=? l') eqn: Heq.
  * destruct l eqn:Hl.
    + destruct l' eqn:Hl'.
      - assert (Hs : s = EmptyPipe). { apply EmptyState in Hl. exact Hl. }
        assert (Hs' : s' = EmptyPipe). { apply EmptyState in Hl'. exact Hl'. }
        assert (Hs'2 : s' <> EmptyPipe). { apply (NotEmpty cnt pc d0 i0 s p)in Hs. subst s'. exact Hs. }
        rewrite Hs' in Hs'2. 

Admitted.

(*** Theorem1: SIC paper ***)
Theorem Monotonic : forall (cnt : nat) (pc : nat)(d: DChit)(i : IChit)(s1 s2 : State)(p : Prog), 
let l1 := length s1.(PRE) in
let l2 := length s2.(PRE) in 
let l1' := length (Update s1 cnt pc p d i).(PRE) in
let l2' := length (Update s2 cnt pc p d i).(PRE) in
beta (ltac2: (let fn := unfold_fixpoint_once constr:(StateProgress) in exact $fn) l1 l2 s1 s2) = Same \/ 
beta (ltac2: (let fn := unfold_fixpoint_once constr:(StateProgress) in exact $fn) l1 l2 s1 s2) = ST1 -> 
beta (ltac2: (let fn := unfold_fixpoint_once constr:(StateProgress) in exact $fn) l1' l2' (Update s1 cnt pc p d i) (Update s2 cnt pc p d i)) = Same \/
beta (ltac2: (let fn := unfold_fixpoint_once constr:(StateProgress) in exact $fn) l1' l2' (Update s1 cnt pc p d i) (Update s2 cnt pc p d i)) = ST1.
Proof.
intros.
left.
destruct H.
  * apply (ProgressDependence cnt pc p d0 i0 s1 s2 ) in H.
    rewrite H. 
    { assert (Heq : l1' = l2'). { subst l1'. subst l2'. rewrite H. reflexivity. } 
    rewrite Heq.
    assert (He : (l2' <=?l2') = true). { rewrite Nat.leb_le. reflexivity. }
    rewrite He.    
      + destruct l2' eqn:Hl2'.
        - reflexivity.
        - assert (Hsi : InstrProgress (n + 1) (Update s2 cnt pc p d0 i0) (Update s2 cnt pc p d0 i0) = Same). { apply SSSI. reflexivity. }
          rewrite Hsi.
Admitted.


(** Second syntax

(*** Theorem1: SIC paper ***)
Theorem Monotonic : forall (cnt : nat) (pc : nat)(d: DChit)(i : IChit)(s1 s2 : State)(p : Prog), 
let l1 := length s1.(PRE) in
let l2 := length s2.(PRE) in 
let l1' := length (Update s1 cnt pc p d i).(PRE) in
let l2' := length (Update s2 cnt pc p d i).(PRE) in
(StateProgress l1 l2 s1 s2 = Same) \/ (StateProgress l1 l2 s1 s2 = ST1) -> 
((StateProgress l1' l2' (Update s1 cnt pc p d i) (Update s2 cnt pc p d i) = Same) \/ (StateProgress l1' l2' (Update s1 cnt pc p d i) (Update s2 cnt pc p d i)) = ST1).
Proof.
intros.
left.
destruct H.
  * apply ProgressDependence in H.
Admitted.
** )
