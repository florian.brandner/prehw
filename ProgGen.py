import itertools
import math
import random
import os
import shutil

############################### Variables ##################################
ProgramLength = 4
Program = []
# N: number of real models to be retreived through function Num_comb
N_models = 4536
 #7185024 #7185024 
# 3/216, 4/4536, 5/149688, 6/7185024, 7/474211584, 8/41256407808
Programs = [[]] * N_models
R0 = set()
R12 = set()

Types_comb = 0
k = 0

Instr_dep = {}
Deps = []

############################### Instr Types ##################################
SetInstr = {"load", "store", "alu"} 

############################### Registers ####################################
Registers = {"ra","rb","rc", "rd"}

########################### Instr format ##################################
def Instruction(index, type, r0, r1, r2):
    Instr = {
        "ind": index,
        "type": type,
        "r0": r0,
        "r1": r1,
        "r2": r2,
        "addr": "0x"+str(index)
    }
    return Instr

########################### MC.tla content ##################################
content1 = """
---- MODULE MC ----
EXTENDS SIC_pipeline, TLC

\* CONSTANT definitions @modelParameterConstants:0missLat
const_172890477841756000 == 
3
----

\* CONSTANT definitions @modelParameterConstants:1lat
const_172890477841757000 == 
[alu |-> 2, load |-> 1, store |-> 2]
----

\* CONSTANT definitions @modelParameterConstants:2mayDMiss
const_172890477841758000 == 
{1}
----

\* CONSTANT definitions @modelParameterConstants:3name
const_172890477841759000 == 
"Test"
----

\* CONSTANT definitions @modelParameterConstants:5mayIMiss
const_172890477841761000 == 
{}
----

\* CONSTANT definitions @modelParameterConstants:6bothDMiss
const_172890477841762000 == 
{2}
----

\* CONSTANT definitions @modelParameterConstants:7bothIMiss
const_172890477841763000 == 
{3}
----

\* CONSTANT definitions @modelParameterConstants:4program
const_172890477841760000 == 
"""
########################### MC.tla content ##################################

cfg = """
\* CONSTANT definitions
CONSTANT
missLat <- const_172890477841756000
\* CONSTANT definitions
CONSTANT
lat <- const_172890477841757000
\* CONSTANT definitions
CONSTANT
mayDMiss <- const_172890477841758000
\* CONSTANT definitions
CONSTANT
name <- const_172890477841759000
\* CONSTANT definitions
CONSTANT
program <- const_172890477841760000
\* CONSTANT definitions
CONSTANT
mayIMiss <- const_172890477841761000
\* CONSTANT definitions
CONSTANT
bothDMiss <- const_172890477841762000
\* CONSTANT definitions
CONSTANT
bothIMiss <- const_172890477841763000
\* SPECIFICATION definition
SPECIFICATION
Spec
\* INVARIANT definition
INVARIANT
NoTA
"""

########################### Initial Program ##################################
for instr in range(ProgramLength):
    Program.append(Instruction(instr+1, "", "", "",""))
    if instr%4 == 0:
        Program[instr]['r0'] = "ra"
    if instr%4 == 1:
        Program[instr]['r0'] = "rb"
    if instr%4 == 2:
        Program[instr]['r0'] = "rc"
    if instr%4 == 3:
        Program[instr]['r0'] = "rd"
    R0.add(Program[instr]["r0"])
#print(Program)

###################### Fcts to generate combinations ############################

def generate_types():
    Types = list(itertools.product(SetInstr, repeat=ProgramLength))
    Types = [list(t) for t in Types if t[0] != "alu"] #Filter alu from options in instr1, since it experiences a D-cache hit/miss
    Types = [list(t) for t in Types if t[1] != "alu"]
    len_types = len(Types)
    print("Num of Type combinations:", len_types)
    return Types, len_types

"""
Dependencies of instr j on instr i, i<j
RAR: j shares same source reg as i => no hazars, ex i=load/j=load
WAR: j's destination reg is one of i's source reg => Not present in SIC, ex j=store/i=load
RAW: one of j's source reg is i's destination reg
WAW: j shares the same destination register as i => Not present in SIC, ex i=store/j=store

Cases of no dep:
1) (i=branch or nop) and (j=branch or nop)
2) (i=load) and (j=load)
3) (i=store)

Cases of dep:
1) (i=load) and (j=store or ALU)
2) (i=ALU) and (j=store or ALU)

PS: to generate all dep combinations
exp length=4:
Pos_dep = {1: {}, 2: {{},{1}}, 3:{{},{1},{2},{1,2}}, 4:{{},{1},{2},{3},{1,2},{1,3},{2,3}} } 
=> Cartesian product of these possibilities while respecting pruning 
"""
def generate_deps():
    for i in range(ProgramLength):
        dependencies = set()
        dependencies.add('NoDep') # First instr is always NoDep
        for j in range(i):
            dependencies.add(j)
        dep1 = list(itertools.combinations(dependencies, 1))
        dep2 = list(itertools.combinations(dependencies, 2))
        dep2_filtered = [comb for comb in dep2 if 'NoDep' not in comb] #take off comb with only 'NoDep' already in dep1
        Instr_dep[i] = dep1 + dep2_filtered
    Deps_lists = [Instr_dep[key] for key in Instr_dep.keys()]
    Deps = list(itertools.product(Deps_lists[0], Deps_lists[1], Deps_lists[2] , Deps_lists[3]))#, Deps_lists[4], Deps_lists[5], Deps_lists[6], Deps_lists[7], Deps_lists[8]))# Deps_lists[9]))#, Deps_lists[10], Deps_lists[11]))
    len_deps = len(Deps)
    print("Len of deps:", len_deps)
    return Deps,len_deps

###################### Generate Program ############################
Prog_types, Prog_types_comb = generate_types()
Prog_deps, Prog_deps_comb = generate_deps()

#Generate one combination of program given a chosen types and deps combinations
def Gen_prog(T_comb,D_comb): #Example: Prog_types[T_comb=0-81], Prog_deps[D_comb=0-56]
    k=0
    l=0
    P = "<<"
    for T in Prog_types[T_comb]:
        Program[k]["type"] = T
        k+=1
    for D in Prog_deps[D_comb]: #dep per instr
        Program[l]['r1']=""
        Program[l]['r2']=""
        if l!=0:
            for m in range(l):
                if (Program[m]["type"]=='alu' and Program[l]["type"]=='alu') or (Program[m]["type"]=="alu" and Program[l]["type"]=='load'): #(Program[m]["type"]=='store' and Program[l]["type"]=="alu") or  or (Program[m]["type"]=="alu" and Program[l]["type"]=="alu"):
                            
                    if(m in D and Program[l]['r1']==""):
                        Program[l]['r1']=Program[m]['r0']

                    if(m in D and Program[l]['r1']!=Program[m]['r0'] and Program[l]['r2']==""):
                        Program[l]['r2']=Program[m]['r0']
                else:
                            Program[l]['r1']=""
                            Program[l]['r2']=""
        l+=1
    for index, item in enumerate(Program):
        P += ' [ind |-> {}, type |-> "{}", r0 |-> "{}", r1 |-> "{}", r2 |-> "{}", addr |-> "{}"]'.format(item['ind'], item['type'], item['r0'], item['r1'], item['r2'], item['addr'])
        if index < len(Program)-1:
             P += ','
    P += ">>"
    """print("Type comb", T_comb ,",Dep comb", D_comb, ":\n")
    print(P)"""
    return P

def Gen_All_comb():
    Prog_types, Prog_types_comb = generate_types()
    Prog_deps, Prog_deps_comb = generate_deps()
    Possibilities = Prog_types_comb * Prog_deps_comb
    print("Type combs:",Prog_types_comb, "Dep combs:", Prog_deps_comb, "Prog combs:", Possibilities)
    i=0
    for t in range(Prog_types_comb):
        k=0
        for T in Prog_types[t]:
            Program[k]["type"] = T
            k+=1
        for d in range(Prog_deps_comb):
            l=0
            P = "<<"
            for D in Prog_deps[d]: #dep per instr
                Program[l]['r1']=""
                Program[l]['r2']=""
                if l!=0:
                    for m in range(l):
                        if (Program[m]["type"]=='alu' and Program[l]["type"]=='alu') or (Program[m]["type"]=="alu" and Program[l]["type"]=='load'):    
                            if(m in D and Program[l]['r1']==""):
                                Program[l]['r1']=Program[m]['r0']

                            if(m in D and Program[l]['r1']!=Program[m]['r0'] and Program[l]['r2']==""):
                                Program[l]['r2']=Program[m]['r0']
                                #print("hi2")                     
                        else:
                            Program[l]['r1']=""
                            Program[l]['r2']=""
                l+=1
            for index, item in enumerate(Program):
                P += ' [ind |-> {}, type |-> "{}", r0 |-> "{}", r1 |-> "{}", r2 |-> "{}", addr |-> "{}"]'.format(item['ind'], item['type'], item['r0'], item['r1'], item['r2'], item['addr'])
                if index < len(Program)-1:
                    P += ','
            P += ">>"
            Programs[i] = P
            #print(Programs[i], "\n")
            i+=1
    return Programs

    
#print(Program)

###################### Get rid of similar programs ############################

def SimProgs(prog):
    SimProgs = []

    PassedProg = {}
    for i, p in enumerate(prog):
        if tuple(p) in PassedProg:
            SimProgs[PassedProg[tuple(p)]].add(i)
        else:
            PassedProg[tuple(p)] = len(SimProgs)
            SimProgs.append({i})

    # Filter out sets with only one occurrence
    #SimProgs = [ind for ind in SimProgs if len(ind) > 1]

    rp=0
    for p in SimProgs:
        #print("Num :", len(p), "\n", p,"\n")
        rp+=1
        #print(rp, "\n")
    print("real models number: ",rp)

    return SimProgs, rp


###################### Generate Models ############################

def Num_comb():
    x, TC = generate_types()
    y, DC = generate_deps()
    print("Total:", TC*DC)
    return TC*DC

def GenModels():
    # Create folders and files
    Programs = Gen_All_comb()
    #print(Programs)
    RP, NP = SimProgs(Programs)
    #print(NP)
    for i in range(1, NP + 1):
        Model_name = f'M{i}'
        file1 = 'MC.tla'
        file2 = 'MC.cfg'
        folder_path = os.path.join("/home/lilia/THESIS/2PracticalWork/ProgGen/SIC/SIC_models_"+str(ProgramLength)+"Instr", Model_name)
        file_path1 = os.path.join(folder_path, file1)
        file_path2 = os.path.join(folder_path, file2)

        # Create folder
        os.makedirs(folder_path, exist_ok=True)

        path1 = "/home/lilia/THESIS/2PracticalWork/ProgGen/SIC/Procedure/graph_generator_tlc.py"        
        path2 = "/home/lilia/THESIS/2PracticalWork/ProgGen/SIC/Procedure/TAExplSIC.class"  
        path3 = "/home/lilia/THESIS/2PracticalWork/ProgGen/SIC/Procedure/TAExplSIC.tla"  
        path4 = "/home/lilia/THESIS/2PracticalWork/ProgGen/SIC/Procedure/SIC_pipeline.tla" 
        path5 = "/home/lilia/THESIS/2PracticalWork/ProgGen/SIC/Procedure/SIC_instructions.tla" 
        path6 = "/home/lilia/THESIS/2PracticalWork/ProgGen/SIC/Procedure/SIC_pipeline_exec.tla" 

        shutil.copy(path1, folder_path)
        shutil.copy(path2, folder_path)
        shutil.copy(path3, folder_path)
        shutil.copy(path4, folder_path)
        shutil.copy(path5, folder_path)
        shutil.copy(path6, folder_path)

        with open(file_path2, "w") as file:
            file.write(cfg)

        content2 = str(Programs[next(iter(RP[i-1]), None)])+ "\n----\n============================================================================="
        content = content1 + content2
        with open(file_path1, "w") as file:
            file.write(content)
        #print(str(Programs[next(iter(RP[i-1]), None)]), "\n")

################################# Build program #######################################                    
#Gen_All_comb()
#T_comb, T_num = generate_types()
#D_comb, D_num = generate_deps()
N = Num_comb()
print (N)
GenModels()

