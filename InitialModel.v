Print eq.
Print True.
Print False.
Require Import Coq.Arith.PeanoNat.
Require Import Lists.List.
Import ListNotations.
Require Import Coq.Arith.Arith.

Inductive Kind := 
  | LD 
  | ST 
  | ALU.

Record Instr := {
  kind : Kind;
  lat : nat 
  }.

Definition Prog := list Instr.

(***************************** INITIAL *********************************)
Module Init.
Record State := {
  inst : Instr;
  cnt : nat;
  prog : Prog
  }.

Definition Step (s: State) : State :=
  if  s.(cnt) =? s.(inst).(lat) then 
  match s.(prog) with 
  | nil => s
  | cons instr prog' => Build_State instr 0 prog'
  end
  else Build_State s.(inst) (s.(cnt)+1) s.(prog).

Fixpoint Run (s : State) ( cycles : nat) : State :=
  match cycles with
  | O => s
  | S cycles' => Run (Step s) cycles'
  end.

Compute Run (Build_State (Build_Instr LD 3) 0 []) 5.
 
End Init.

(***************************** IMP1: basic *********************************)

Check leb.

Module Imp1.
Record State := {
  inst : Instr;
  cnt : nat;
  prog : Prog
  }.

Definition Step (s: State) : option State :=
  
  (* Check first if the s is a correct state *)
  if leb s.(cnt) s.(inst).(lat) then 
    (* if so compute new state *)
    if s.(cnt) =? s.(inst).(lat) then
      match s.(prog) with 
      | nil => Some s
      | cons instr prog' => Some (Build_State instr 0 prog')
      end
    else Some(Build_State s.(inst) (s.(cnt)+1) s.(prog))
  
  (* else return Error *)
  else None.


Fixpoint Run (s : option State) ( cycles : nat) : option State :=
  match s with
  | None => None
  | Some s' =>  
    match cycles with
    | O => Some s'
    | S cycles' => Run (Step s') cycles'
    end
  end.

Definition st : State := Build_State (Build_Instr LD 3) 0 [].

Compute Run (Some st) 2. 

Definition st2 : State := Build_State (Build_Instr LD 3) 4 [].

Compute Run (Some st2) 1.
End Imp1.

(***************************** IMP2: Dep type *********************************)
Module Imp2.
Record State := {
  inst : Instr;
  cnt : nat;
  prog : Prog;
  CounterOK : cnt <= inst.(lat)
  }.

Check CounterOK.
Check Build_State.

Compute Build_Instr LD 3.

Definition ist : Instr := Build_Instr LD 3.
Lemma istb : 0 <= ist.(lat).
Proof.
simpl.
apply le_S.
apply le_S.
apply le_S.
Search (_ <= S _).
apply le_n.
Qed.

Definition st : State := Build_State (ist) 0 [] istb.
Print st.

Require Import Program.

Search eq nat "dec".
Print sumbool.
(* Using match *)

Lemma StepProof : forall (s : State), cnt s <> lat (inst s)  ->  cnt s + 1 <= lat (inst s).
Proof.
intros s Hneq. 
destruct s.
simpl in *.
Search (_ <> _) nat le.  
assert (H1: cnt0 < lat inst0).
+ apply Nat.le_neq.
  split.
  * apply CounterOK0.  
  * apply Hneq. 
+ Search (S _ <= _) lt. 
rewrite <- Nat.le_succ_l in H1.
Search (_ + 1 = S _).
rewrite Nat.add_1_r.
apply H1.
Qed.

Definition Step (s: State) : State := 
  match Nat.eq_dec s.(cnt) s.(inst).(lat) with
  | left Heq => 
    match s.(prog) with 
    | nil => s
    | cons instr prog' => Build_State instr 0 prog' (Nat.le_0_l instr.(lat))
    end
  | right Hneq => Build_State s.(inst) (s.(cnt)+1) s.(prog) (StepProof s Hneq)
  end. 
(*Next Obligation.
Search ( 0 <= _).
apply Nat.le_0_l.
Qed.*)


(*Program Definition Step (s: State) : State :=
  if leb s.(cnt) s.(inst).(lat) then
  match s.(prog) with 
    | nil => s
    | cons instr prog' => Build_State instr 0 prog' (Nat.le_0_l instr.(lat))
  end
  else Build_State s.(inst) (s.(cnt)+1) s.(prog) _.

(*Next Obligation.
Search ( 0 <= _).
apply Nat.le_0_l.
Qed.*)
Next Obligation. 
destruct s.
simpl in *.
Search (_ <= _).  
assert (H: cnt0 < lat inst0).
+ apply Nat.le_neq.
  split.
  * apply CounterOK0. 
  * Search(_ <> _)(_ <= _). 
+ Search (S _ <= _) lt. 
rewrite <- Nat.le_succ_l in H.
Search (_ + 1 = S _).
rewrite Nat.add_1_r.
apply H.
Qed.*)

Fixpoint Run (s : State) ( cycles : nat) : State :=
  match cycles with
  | O => s
  | S cycles' => Run (Step s) cycles'
  end.
End Imp2.

(***************************** MAIN *********************************)
Check Init.State.
Check Imp1.st.
Check Imp1.st.(Imp1.cnt).
Check Imp1.st.(Imp1.inst).
Check Imp1.st.(Imp1.inst).(lat).

(**************************************** PROOF1 ***************************************************)
                         (* in Imp1: A valid state -> a valid state *)

Definition Correct : forall (s1: Imp1.State), 
s1.(Imp1.cnt) <= s1.(Imp1.inst).(lat) -> exists s, Imp1.Step s1 = Some s.
Proof.
intros.

assert (H1: s1.(Imp1.cnt) <= s1.(Imp1.inst).(lat) -> leb s1.(Imp1.cnt) s1.(Imp1.inst).(lat) = true).
{ intros. 
  apply Nat.leb_le in H0.
  exact H0.
}

unfold Imp1.Step.
rewrite H1.
+

  assert (H2: s1.(Imp1.cnt) <= s1.(Imp1.inst).(lat) -> 
  (Nat.eqb s1.(Imp1.cnt) s1.(Imp1.inst).(lat) = true) \/ (Nat.ltb s1.(Imp1.cnt) s1.(Imp1.inst).(lat) = true)).
  { intros.
  destruct (Nat.eqb_spec s1.(Imp1.cnt) s1.(Imp1.inst).(lat)) as [Heq | Hneq]. (* Case analysis on a = b *)
  - left. 
    reflexivity.
  - right.
    Search (_ <? _ = true) nat.
    apply Nat.ltb_lt.
    Search (_ < _) (_ <= _) (_ <> _) nat.
    apply Nat.le_neq.
    split.
    * exact H.
    * exact Hneq.
  }

  destruct H2.
  * exact H.
  * rewrite H0. 
    destruct Imp1.prog.
    { exists s1. reflexivity. }
    { exists {| Imp1.inst := i; Imp1.cnt := 0; Imp1.prog := p |}. reflexivity. }
  * destruct (Nat.eqb_spec (Imp1.cnt s1) (lat (Imp1.inst s1))) as [H2 | H2].
    { destruct Imp1.prog.
      { exists s1. reflexivity. }
      { exists {| Imp1.inst := i; Imp1.cnt := 0; Imp1.prog := p |}. reflexivity. } }
    { exists {| Imp1.inst := Imp1.inst s1; Imp1.cnt := Imp1.cnt s1 + 1; Imp1.prog := Imp1.prog s1 |}. reflexivity. }

+ exact H.
Qed.

(* NEW PROOF 1*)
Definition Correct' : forall (s: Imp1.State), 
s.(Imp1.cnt) <= s.(Imp1.inst).(lat) -> exists s', (Imp1.Step s = Some s') /\ (s'.(Imp1.cnt) <= s'.(Imp1.inst).(lat)). 
Proof.
intros s H.
unfold Imp1.Step. 
assert (H1: (Imp1.cnt s <=? lat (Imp1.inst s))=true). { apply Nat.leb_le in H. exact H. }
rewrite H1. 
destruct (Imp1.cnt s =? lat (Imp1.inst s)) eqn:H2.
destruct (Imp1.prog s).
* exists s. split. reflexivity. exact H.
* set (s':={| Imp1.inst := i; Imp1.cnt := 0; Imp1.prog := p |}). exists s'. split. reflexivity.
  assert (H3: Imp1.cnt s' = 0). { reflexivity. } assert (H4: Imp1.inst s' = i). { reflexivity. }  
  rewrite H3, H4. apply Nat.le_0_l. 
* set (s':= {| Imp1.inst := Imp1.inst s; Imp1.cnt := Imp1.cnt s + 1; Imp1.prog := Imp1.prog s|}). exists s'. split. reflexivity. 
  assert (H3: Imp1.cnt s < lat (Imp1.inst s)). { apply EqNat.beq_nat_false_stt in H2. apply Nat.le_neq. split. exact H. exact H2. }
  assert (H4: Imp1.cnt s' = Imp1.cnt s + 1). { reflexivity. } assert (H5: lat(Imp1.inst s') = lat(Imp1.inst s)). { reflexivity. }
  rewrite H4, H5. 
  assert (H6: Imp1.cnt s + 1 = S (Imp1.cnt s)). { apply Nat.add_1_r. } rewrite H6.
  apply Nat.le_succ_l. exact H3. 
Qed.

(**************************************** END PROOF1 ************************************************)

(**************************************** PROOF2 ****************************************************)
                (* Imp1.Step <-> Imp2.Step in case of {Imp1 correct state} *)


(****************** Need some conversion part when comparing Imp1 and Imp2 **************************)
(* Imp2.State -> Imp1.State *)
Definition ConvertState (s: Imp2.State) : Imp1.State := 
Imp1.Build_State s.(Imp2.inst) s.(Imp2.cnt) s.(Imp2.prog).

(* Imp2.Step -> Imp1.Step *)
Definition ConvertStep (s: Imp2.State) : option Imp1.State := 
Some (ConvertState s).
(*Some(Imp1.Build_State s.(Imp2.inst) s.(Imp2.cnt) s.(Imp2.prog)).*)

Print Imp1.st.
Print Imp2.st. 
Compute ConvertState Imp2.st.
Compute Imp1.Step Imp1.st.
Compute ConvertStep (Imp2.Step Imp2.st).

(********************** Mini proof1: Imp1.State <-> Imp2.State for correct state ********************)
Lemma eqState : forall (s1: Imp1.State) (s2: Imp2.State), 
s1.(Imp1.cnt) <= s1.(Imp1.inst).(lat) /\ s1 = ConvertState s2 -> s2.(Imp2.cnt) <= s2.(Imp2.inst).(lat).
Proof.
intros.
destruct H.
unfold ConvertState in H0.
rewrite H0 in H.
simpl in H.
exact H.
Qed.

(******************************************* Mini proof2 ********************************************)
Lemma some_injective : forall (A : Type) (x y : A),
  Some x = Some y -> x = y.
Proof.
  intros A x y H.
  injection H as Hxy.
  apply Hxy.
Qed.

Lemma eqSome1 : forall (st1 st2: Imp1.State), Some st1 = Some st2 -> st1 = st2.
Proof.
intros. apply some_injective. exact H.
Qed.

Lemma eqSome2 : forall (st1 st2: Imp1.State), st1 = st2 -> Some st1 = Some st2.
Proof.
  intros s1 s2 H.
  rewrite H.
  reflexivity.
Qed.

(******************************************* Mini proof3 ********************************************)
Lemma eqs12 : forall (s1 s2: Imp1.State), s1 = s2 -> 
(s1.(Imp1.inst) = s2.(Imp1.inst) /\ s1.(Imp1.cnt) = s2.(Imp1.cnt) /\ s1.(Imp1.prog) = s2.(Imp1.prog)).
Proof.
intros. destruct s1, s2. inversion H. split. reflexivity. split. reflexivity. reflexivity. Qed.

Lemma Imp1st2eq : forall (s1: Imp1.State) (s2: Imp2.State), s1 = ConvertState s2 -> Imp1.Step s1 = Imp1.Step (ConvertState s2). (*PS: THIS SHOULD BE AN EQUIVALENCE*)
Proof.
intros.
rewrite H. reflexivity.
Qed.

Lemma Imp1st1eq : forall (s1 s2: Imp1.State), s1 = s2 -> Imp1.Step s1 = Imp1.Step s2. (*PS: THIS SHOULD BE AN EQUIVALENCE*)
Proof.
intros.
rewrite H. reflexivity.
Qed.

(*** NEW PROOF2 ***)
Lemma Imp1stepeq : exists (s1 s2: Imp1.State), Imp1.Step s1 = Imp1.Step s2 /\ (s1 <> s2). 
Proof.
exists {| Imp1.inst := ((Build_Instr LD 3)); Imp1.cnt := 3; Imp1.prog := [(Build_Instr ST 2)] |}, {| Imp1.inst := ((Build_Instr LD 2)); Imp1.cnt := 2; Imp1.prog := [(Build_Instr ST 2)] |}.
split. reflexivity.
unfold not. discriminate.
Qed.

Lemma eqState12 : forall (s1: Imp1.State) (s2: Imp2.State),
(s1.(Imp1.inst) = s2.(Imp2.inst) /\ s1.(Imp1.cnt) = s2.(Imp2.cnt) /\ s1.(Imp1.prog) = s2.(Imp2.prog)) -> s1 = ConvertState s2.
Proof.
intros.
destruct H. destruct H0.
unfold ConvertState. 
destruct s1.
destruct s2. simpl in *.
rewrite <- H. rewrite <-H0. rewrite <- H1.
reflexivity.
Qed.

(***************************************** START OF PROOF2 ******************************************)

(* for a given identical state Imp2.state -> Imp1.Step = Imp2.Step *)
Lemma eqStepst2 : forall (s: Imp2.State), Imp1.Step (ConvertState s) = ConvertStep (Imp2.Step s).
Proof.
intros.
(* unfold Imp2.Step. *)
(* destruct s as [i l p]. *)
unfold Imp1.Step. simpl.
unfold Imp2.Step. simpl. 
assert(H: (Imp2.cnt s <=? lat (Imp2.inst s)) = true). { apply Nat.leb_le. apply s.(Imp2.CounterOK). }
rewrite H. 
destruct (Nat.eq_dec (Imp2.cnt s) (lat (Imp2.inst s))) as [Heq| Hneq];
destruct (Imp2.cnt s =? lat (Imp2.inst s)) eqn:H2.
  2:{ apply EqNat.beq_nat_false_stt in H2. contradiction. }
  2:{ apply Nat.eqb_eq in H2. contradiction. }
  (* rewrite Heq, Nat.leb_refl. *) destruct (Imp2.prog s) eqn:H3; reflexivity.
  (* Check Nat.leb_le. Print iff. *)
  (* rewrite (proj2 (Nat.leb_le s.(Imp2.cnt) (lat(Imp2.inst s))) s.(Imp2.CounterOK)). *)
  reflexivity.
Qed.

Lemma eqStep12 : forall (s1: Imp1.State) (s2: Imp2.State), s1 = ConvertState s2 -> Imp1.Step s1 = ConvertStep (Imp2.Step s2).
Proof.
intros.
rewrite H.
apply eqStepst2.
Qed.


