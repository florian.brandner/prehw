import sys, re
import networkx as nx
import pickle

missLat = 3

def get_content(raw):
    graph1 = re.findall(r'graph (?:\|->|=) (\[ nodes(?:.|\s)*?\})', raw)[-1]
    graph2 = re.findall(r'graph2 (?:\|->|=) (\[ nodes(?:.|\s)*?\})', raw)[-1]
    return (graph1, graph2)

def parse_graph(content):
    N_instr = 0
    nodes = list()
    line_edges = list()
    MEMmiss = []
    IFmiss = []
    EXlat = []
    MEMlat = []
    nodes_input = re.search(r'nodes (?:\|->|=)(?:.|\n)*<< ((?:.|\n)+) >>', content)
    #edges_input = re.search(r'edges (?:\|->|=)(?:.|\n)*\{ ((?:.|\n)+) \}', content)
    edges_input = re.search(r'edges\s*\|->\s*{(.+?)}', content, re.DOTALL)
    #print("INPUT", edges_input)
    if nodes_input != None:
        lines = re.findall(r'\[((?:.|\n)+?)\]', nodes_input.group(1))
        N_instr = len(lines)
        #print("INSTR1\n", N_instr)
        line_edges = [[] for i in range(N_instr)]
        
        for i in range(len(lines)):
            reg = re.search(r'IFacq (?:\|->|=) (\d+)', lines[i])
            nodes.append({'IFacq' : reg.group(1)})
            reg = re.search(r'IFrel (?:\|->|=) (\d+)', lines[i])
            nodes[i]['IFrel'] = reg.group(1)
            reg = re.search(r'IDacq (?:\|->|=) (\d+)', lines[i])
            nodes[i]['IDacq'] = reg.group(1)
            reg = re.search(r'IDrel (?:\|->|=) (\d+)', lines[i])
            nodes[i]['IDrel'] = reg.group(1)
            reg = re.search(r'EXacq (?:\|->|=) (\d+)', lines[i])
            nodes[i]['EXacq'] = reg.group(1)
            reg = re.search(r'EXrel (?:\|->|=) (\d+)', lines[i])
            nodes[i]['EXrel'] = reg.group(1)
            reg = re.search(r'MEMacq (?:\|->|=) (\d+)', lines[i])
            nodes[i]['MEMacq'] = reg.group(1)
            reg = re.search(r'MEMrel (?:\|->|=) (\d+)', lines[i])
            nodes[i]['MEMrel'] = reg.group(1)
            reg = re.search(r'STacq (?:\|->|=) (\d+)', lines[i])
            #reg = re.search(r'STacq (?:\|->|=) ([1-9]+)', lines[i])
            #if reg != None:
            nodes[i]['STacq'] = reg.group(1)
            reg = re.search(r'STrel (?:\|->|=) (\d+)', lines[i])
            #reg = re.search(r'STrel (?:\|->|=) ([1-9]+)', lines[i])
            #if reg!=None:
            nodes[i]['STrel'] = reg.group(1)
            reg = re.search(r'WBacq (?:\|->|=) (\d+)', lines[i])
            #reg = re.search(r'WBacq (?:\|->|=) ([1-9]+)', lines[i])
            #if reg!=None:
            nodes[i]['WBacq'] = reg.group(1)
            reg = re.search(r'WBrel (?:\|->|=) (\d+)', lines[i])
            #reg = re.search(r'WBrel (?:\|->|=) ([1-9]+)', lines[i])
            #if reg!=None:
            nodes[i]['WBrel'] = reg.group(1)
            reg = re.search(r'ind (?:\|->|=) (\d+)', lines[i])
            nodes[i]['ind'] = reg.group(1)
            reg = re.search(r'addr (?:\|->|=) "(0x[0-9a-f]+)"', lines[i])
            nodes[i]['addr'] = reg.group(1)
            reg = re.search(r'IFmiss (?:\|->|=) (\d+)', lines[i])
            IFmiss.append(reg.group(1))
            print("IFMISS", IFmiss)
            reg = re.search(r'MEMmiss (?:\|->|=) (\d+)', lines[i])
            MEMmiss.append(reg.group(1))
            reg = re.search(r'EXlat (?:\|->|=) (\d+)', lines[i])
            EXlat.append(reg.group(1))
            reg = re.search(r'MEMlat (?:\|->|=) (\d+)', lines[i])
            MEMlat.append(reg.group(1))

    if edges_input != None:
        #print("NOT NONE")
        edges = re.findall(r'\[(.+?)\]', edges_input.group(1))
        ind_list = [x['ind'] for x in nodes]
        
        for i in range(len(edges)):
            reg = re.search(r'source (?:\|->|=) (\d+)', edges[i])
            if reg.group(1) not in ind_list or ind_list.index(reg.group(1)) >= N_instr: continue
            source = ind_list.index(reg.group(1))
            reg = re.search(r'dest (?:\|->|=) (\d+)', edges[i])
            if reg.group(1) not in ind_list or ind_list.index(reg.group(1)) >= N_instr: continue
            dest = ind_list.index(reg.group(1))
            reg = re.search(r'type (?:\|->|=) "(\w+)"', edges[i])
            _type = reg.group(1)
            line_edges[source].append({'dest' : dest, 'type' : _type})
    return (N_instr, nodes, line_edges, IFmiss, MEMmiss, MEMlat, EXlat)
    
def build_graph(N_instr, nodes, edges, IFmiss, MEMmiss, MEMlat, EXlat):
    g = nx.MultiDiGraph()
    istr = lambda i : nodes[i]['addr']
    for i in range(N_instr):
        g.add_node(str(i)+'upIF', label='('+istr(i)+',&uarr;IF,'+nodes[i]['IFacq']+')', instr=i, evt='upIF', t=int(nodes[i]['IFacq']), ind=istr(i))
        g.add_node(str(i)+'dwIF', label='('+istr(i)+',&darr;IF,'+nodes[i]['IFrel']+')', instr=i, evt='dwIF', t=int(nodes[i]['IFrel']), ind=istr(i))
        g.add_node(str(i)+'upID', label='('+istr(i)+',&uarr;ID,'+nodes[i]['IDacq']+')', instr=i, evt='upID', t=int(nodes[i]['IDacq']), ind=istr(i))
        g.add_node(str(i)+'dwID', label='('+istr(i)+',&darr;ID,'+nodes[i]['IDrel']+')', instr=i, evt='dwID', t=int(nodes[i]['IDrel']), ind=istr(i))
        g.add_node(str(i)+'upEX', label='('+istr(i)+',&uarr;EX,'+nodes[i]['EXacq']+')', instr=i, evt='upEX', t=int(nodes[i]['EXacq']), ind=istr(i))
        g.add_node(str(i)+'dwEX', label='('+istr(i)+',&darr;EX,'+nodes[i]['EXrel']+')', instr=i, evt='dwEX', t=int(nodes[i]['EXrel']), ind=istr(i))
        g.add_node(str(i)+'upMEM', label='('+istr(i)+',&uarr;MEM,'+nodes[i]['MEMacq']+')', instr=i, evt='upMEM', t=int(nodes[i]['MEMacq']), ind=istr(i))
        g.add_node(str(i)+'dwMEM', label='('+istr(i)+',&darr;MEM,'+nodes[i]['MEMrel']+')', instr=i, evt='dwMEM', t=int(nodes[i]['MEMrel']), ind=istr(i))
        if int(nodes[i]['STacq'])!=0:
            g.add_node(str(i)+'upST', label='('+istr(i)+',&uarr;ST,'+nodes[i]['STacq']+')', instr=i, evt='upST', t=int(nodes[i]['STacq']), ind=istr(i))
        if int(nodes[i]['STrel'])!=0:
            g.add_node(str(i)+'dwST', label='('+istr(i)+',&darr;ST,'+nodes[i]['STrel']+')', instr=i, evt='dwST', t=int(nodes[i]['STrel']), ind=istr(i))
        if int(nodes[i]['WBacq'])!=0:
            g.add_node(str(i)+'upWB', label='('+istr(i)+',&uarr;WB,'+nodes[i]['WBacq']+')', instr=i, evt='upWB', t=int(nodes[i]['WBacq']), ind=istr(i))
        if int(nodes[i]['WBrel'])!=0:
            g.add_node(str(i)+'dwWB', label='('+istr(i)+',&darr;WB,'+nodes[i]['WBrel']+')', instr=i, evt='dwWB', t=int(nodes[i]['WBrel']), ind=istr(i))
 
        # Rule 1: Order of pipeline stages
        g.add_edge(str(i)+'dwIF', str(i)+'upID', xlabel=' 0 ', t=0, style='bold')
        g.add_edge(str(i)+'dwID', str(i)+'upEX', xlabel=' 0 ', t=0, style='bold')    
        g.add_edge(str(i)+'dwEX', str(i)+'upMEM', xlabel=' 0 ', t=0, style='bold')
        if int(nodes[i]['STacq'])!=0:
            g.add_edge(str(i)+'dwMEM', str(i)+'upST', xlabel=' 0 ', t=0, style='bold')
        if int(nodes[i]['WBacq'])!=0:
            g.add_edge(str(i)+'dwMEM', str(i)+'upWB', xlabel=' 0 ', t=0, style='bold') 

        # Rule 2: Res usage
        if IFmiss[i] == '1' or IFmiss[i] == '2':
            g.add_edge(str(i)+'upIF', str(i)+'dwIF', xlabel=' '+str(missLat)+' ', t=missLat, style='bold')
        else:
            g.add_edge(str(i)+'upIF', str(i)+'dwIF', xlabel=' '+str(1)+' ', t=1, style='bold')
        g.add_edge(str(i)+'upID', str(i)+'dwID', xlabel=' '+str(1)+' ', t=1, style='bold')
        g.add_edge(str(i)+'upEX', str(i)+'dwEX', xlabel=' '+str(EXlat[i])+' ', t=int(EXlat[i]), style='bold')
        if MEMmiss[i] == '1' or MEMmiss[i] == '2':
            if int(nodes[i]['STacq'])!=0:
                g.add_edge(str(i)+'upMEM', str(i)+'dwMEM', xlabel=' '+str(1)+' ', t=1, style='bold')
            else:
                g.add_edge(str(i)+'upMEM', str(i)+'dwMEM', xlabel=' '+str(missLat)+' ', t=missLat, style='bold')
        else:
            if int(nodes[i]['STacq'])!=0:
                g.add_edge(str(i)+'upMEM', str(i)+'dwMEM', xlabel=' '+str(1)+' ', t=1, style='bold')
            else:
                g.add_edge(str(i)+'upMEM', str(i)+'dwMEM', xlabel=' '+str(MEMlat[i])+' ', t=int(MEMlat[i]), style='bold')
        if int(nodes[i]['STacq'])!=0:
            if MEMmiss[i] == '1' or MEMmiss[i] == '2':
                g.add_edge(str(i)+'upST', str(i)+'dwST', xlabel=' '+str(missLat)+' ', t=missLat, style='bold')#missLat-1???
            else:
                g.add_edge(str(i)+'upST', str(i)+'dwST', xlabel=' '+str(MEMlat[i])+' ', t=int(MEMlat[i]), style='bold')
        if int(nodes[i]['WBacq'])!=0:
            g.add_edge(str(i)+'upWB', str(i)+'dwWB', xlabel=' '+str(1)+' ', t=1, style='bold')
    
        # Rule 3: Order of instr
        if i > 0:
            g.add_edge(str(i-1)+'upIF', str(i)+'upIF', xlabel=' '+str(int(nodes[i]['IFacq'])-int(nodes[i-1]['IFacq']))+' ', t=int(nodes[i]['IFacq'])-int(nodes[i]['IFacq']))
            g.add_edge(str(i-1)+'upID', str(i)+'upID', xlabel=' '+str(int(nodes[i]['IDacq'])-int(nodes[i-1]['IDacq']))+' ', t=int(nodes[i]['IDacq'])-int(nodes[i]['IDacq']))
            g.add_edge(str(i-1)+'upEX', str(i)+'upEX', xlabel=' '+str(int(nodes[i]['EXacq'])-int(nodes[i-1]['EXacq']))+' ', t=int(nodes[i]['EXacq'])-int(nodes[i]['EXacq']))
            g.add_edge(str(i-1)+'upMEM', str(i)+'upMEM', xlabel=' '+str(int(nodes[i]['MEMacq'])-int(nodes[i-1]['MEMacq']))+' ', t=int(nodes[i]['MEMacq'])-int(nodes[i]['MEMacq']))
            if int(nodes[i]['STacq'])!=0 and int(nodes[i-1]['STacq'])!=0:
                g.add_edge(str(i-1)+'upST', str(i)+'upST', xlabel=' '+str(int(nodes[i]['STacq'])-int(nodes[i-1]['STacq']))+' ', t=int(nodes[i]['STacq'])-int(nodes[i]['STacq']))
            if int(nodes[i]['WBacq'])!=0 and int(nodes[i-1]['WBacq'])!=0:
                g.add_edge(str(i-1)+'upWB', str(i)+'upWB', xlabel=' '+str(int(nodes[i]['WBacq'])-int(nodes[i-1]['WBacq']))+' ', t=int(nodes[i]['WBacq'])-int(nodes[i]['WBacq']))
        
        # Rule 4: Dependencies
        for e in edges[i]:
            if e['dest'] < N_instr and e['type'] == 'DL':
                g.add_edge(str(i)+'dwMEM', str(e['dest'])+'upEX', xlabel=' 0 ', t=0, color='red', fontcolor='red')
            if e['dest'] < N_instr and e['type'] == 'DA':
                g.add_edge(str(i)+'dwEX', str(e['dest'])+'upEX', xlabel=' 0 ', t=0, color='red', fontcolor='red')
                
        # Rule 5: Res contention
        #PS : Still needs to be discussed and adapted
        for j in range(N_instr) : 
            if j==i+1 and (int(nodes[i]['EXrel']) - int(nodes[i]['EXacq']) == int(EXlat[i])) and (EXlat[i] != '1') and int(nodes[i]['EXrel'])==int(nodes[j]['EXacq']):
                g.add_edge(str(i)+'dwEX', str(j)+'upEX', xlabel=' 0 ', t=0, color='blue', fontcolor='blue')
            if j==i+1 and int(nodes[i]['MEMrel'])==int(nodes[j]['MEMacq']) and ((int(nodes[i]['MEMrel']) - int(nodes[i]['MEMacq']) == int(MEMlat[i]) and MEMmiss[i] =='0' and (MEMlat[i] != '1')) or (int(nodes[i]['MEMrel']) - int(nodes[i]['MEMacq']) == missLat and MEMmiss[i] =='1'))   : 
                g.add_edge(str(i)+'dwMEM', str(j)+'upMEM', xlabel=' 0 ', t=0, color='blue', fontcolor='blue')

        # Rule 6: Bus access
        for j in range(N_instr) : 
            #MEMPending IF-MEM/IF-ST
            if j>i and (IFmiss[j] == '1' or IFmiss[j] == '2') and (int(nodes[i]['MEMrel']) == int(nodes[j]['IFacq'])) : #hit case
                g.add_edge(str(i)+'dwMEM', str(j)+'upIF', xlabel=' 0 ', t=0, color='green', fontcolor='green')
            if j>i and (IFmiss[j] == '1' or IFmiss[j] == '2') and (int(nodes[i]['STrel']) == int(nodes[j]['IFacq'])):#ST 
                g.add_edge(str(i)+'dwST', str(j)+'upIF', xlabel=' 0 ', t=0, color='green', fontcolor='green')
            #MEM-MEM: case WB/WB 
            if j==i+1 and (MEMmiss[i] == '1' or MEMmiss[i] == '2') and (MEMmiss[j] == '1' or MEMmiss[j] == '2') and (int(nodes[i]['MEMrel']) == int(nodes[j]['MEMacq'])) : #WB or ST hit
                g.add_edge(str(i)+'dwMEM', str(j)+'upMEM', xlabel=' 0 ', t=0, color='green', fontcolor='green')
            #ST-MEM: If write-through policy no need to check MEMmiss for i, bus is accessed either ways with ST 
            if j==i+1 and (MEMmiss[j] == '1' or MEMmiss[j] == '2') and (int(nodes[i]['STrel']) == int(nodes[j]['MEMacq'])) : #WB or ST hit
                g.add_edge(str(i)+'dwST', str(j)+'upMEM', xlabel=' 0 ', t=0, color='green', fontcolor='green')
            #IF-IF: Never happens due to MEMPending rule
            if j==i+1 and (IFmiss[i] == '1' or IFmiss[i] == '2') and (IFmiss[j] == '1' or IFmiss[j] == '2') and (int(nodes[i]['IFrel']) == int(nodes[j]['IFacq'])) : #WB or ST hit
                g.add_edge(str(i)+'dwIF', str(j)+'upIF', xlabel=' 0 ', t=0, color='green', fontcolor='green')

    instants = set()
    for i in range(N_instr):
        for s in ('upIF', 'dwIF', 'upID', 'dwID', 'upEX', 'dwEX', 'upMEM', 'dwMEM', 'upWB', 'dwWB', 'upST', 'dwST'): 
            n = str(i)+s
            if n in g.nodes :
                instants.add(g.nodes[n]['t'])
                #print("GNODES",g.nodes)

    prev_t = -1
    prev_inst = -1
    newline = False
    newlines = 0
    for i in range(N_instr):
        for s in ('upIF', 'dwIF', 'upID', 'dwID', 'upEX', 'dwEX', 'upMEM', 'dwMEM', 'upWB', 'dwWB', 'upST', 'dwST'): 
            n = str(i)+s
            if n in g.nodes : # Allows to filter WB for store instr and ST for the rest
                inst = g.nodes[n]['instr']
                t = g.nodes[n]['t']
                if inst != prev_inst and newline:
                    newlines += 1
                    newline = False
                        
                if t == prev_t:
                    ypos = inst+newlines+1
                    newline = True
                else:
                    ypos = inst+newlines
                
                col = len([tt for tt in instants if tt <= t])
                g.nodes[n]['pos'] = str(3*col)+",-"+str(0.9*ypos)+"!"
                prev_t = t
                prev_inst = inst         

    return g #(g, com_nodes)

def explore(g_cause, N_instr, g_other, IFmiss, MEMmiss, MEMlat, EXlat):
    label = lambda n : str(g_cause.nodes[n]['ind'])+g_cause.nodes[n]['evt']
        
    def has_TA(path, acq, rel): # returns whether there is a variation in the beginning of path in g_cause wrt. g_other and whether this variation entails a TA      
        corrAcq, corrRel = path[-1], path[-2]
        t_acq_cause = g_cause.nodes[acq]['t']
        t_rel_cause = g_cause.nodes[rel]['t']
        t_acq_other = g_other.nodes[corrAcq]['t']
        t_rel_other = g_other.nodes[corrRel]['t']
        delta_cause = t_rel_cause - t_acq_cause
        delta_other = t_rel_other - t_acq_other
        
        #print("TEST", g_cause.nodes[]['ind'], g_cause.nodes[N_instr-1]['evt'], g_cause.nodes[N_instr-1]['t'])
        variation = (delta_cause != delta_other)
        TA = False
        if variation:
        #Checking effect of AMP on global timing
            if (str(N_instr-1)+'dwWB') in g_other.nodes :
                GT_cause = g_cause.nodes[str(N_instr-1)+'dwWB']['t'] - 1
                GT_other = g_other.nodes[str(N_instr-1)+'dwWB']['t'] - 1
            else:
                GT_cause = g_cause.nodes[str(N_instr-1)+'dwST']['t'] - 1
                GT_other = g_other.nodes[str(N_instr-1)+'dwST']['t'] - 1
            
            if GT_other - GT_cause > delta_other - delta_cause:
                print("Standard AMP")

        
            for x in (acq,rel):
                g_cause.nodes[x]['style'] = 'filled'
                g_cause.nodes[x]['fillcolor'] = 'gray'
            for x in (corrAcq,corrRel):
                g_other.nodes[x]['style'] = 'filled'
                g_other.nodes[x]['fillcolor'] = 'gray'
            if delta_cause < delta_other:
                print('\t*Favorable variation: ('+label(acq)+', '+label(rel)+') ('+str(delta_cause)+'<'+str(delta_other)+')')
                for i,x in enumerate(path[:-2]):
                    instr = g_cause.nodes[x]['instr']
                    corrGlob = x #if x in g_other.nodes else str(instr)+'upFU' case of res switch 
                    Delta_cause = g_cause.nodes[x]['t'] - t_rel_cause
                    Delta_other = g_other.nodes[corrGlob]['t'] - t_rel_other
                    
                    if (Delta_cause < Delta_other):
                        Delta_cause_table_repr = str(Delta_cause)
                        Delta_other_table_repr = str(Delta_other)
                        print('\t**TA: ('+label(acq)+', '+label(rel)+')->'+label(x)+' ('+Delta_cause_table_repr+'>'+Delta_other_table_repr+')')
                        g_other.nodes[x]['style'] = 'filled'
                        g_other.nodes[x]['fillcolor'] = '#FC8D59'
                        TA = True
                    elif 'style' not in g_other.nodes[x] or g_other.nodes[x]['style'] != 'filled':
                        g_other.nodes[x]['style'] = 'filled'
                        g_other.nodes[x]['fillcolor'] = '#FDCC8A'
                      
        return variation, TA
        
    def get_paths(path): # Recursively finds causal paths
        n = path[-1]
        print([label(x) for x in path])
        
        for nn in g_other.predecessors(n): 
            res_usage = g_other.nodes[nn]['evt'] in ('upIF', 'upMEM', 'upST') and g_other.nodes[n]['evt'] in ('dwIF', 'dwMEM','dwST')
            print("RES", res_usage)
            acq = nn
            rel = n

            #print("BIG TEST", IFmiss[g_other.nodes[nn]['instr']])
            if g_other.nodes[nn]['evt'] == 'upIF':
                miss_info = (IFmiss[g_other.nodes[nn]['instr']] == '1')
            elif g_other.nodes[nn]['evt'] == 'upMEM' or g_other.nodes[nn]['evt'] == 'upST':
                miss_info = (MEMmiss[g_other.nodes[nn]['instr']] == '1')
            else:
                miss_info = False

            break_path = list()
            for i in range(len(g_other[nn][n])):
                timing_indep = (g_other.nodes[nn]['t'] + g_other[nn][n][i]['t'] != g_other.nodes[n]['t'])
                print ("NN, N, I, G_OTHER(NN,t), G_OTHER(NN,N,I,t), G_OTHER(N,t), T, M", nn, n, i, g_other.nodes[nn]['t'], g_other[nn][n][i]['t'], g_other.nodes[n]['t'], timing_indep, miss_info)
                #Stalls management: Breaking causaily in case of stalls
                """
                if g_other.nodes[nn]['instr'] == g_other.nodes[n]['instr']:
                    #IF stage
                    if g_other.nodes[nn]['evt'] == "upIF" and g_other.nodes[n]['evt'] == "dwIF": 
                        #PB1
                        if IFmiss[g_other.nodes[nn]['instr']] == '0':
                            timing_indep = (g_other[nn][n][i]['t'] != 1)
                        if IFmiss[g_other.nodes[nn]['instr']] == '1':
                            timing_indep = (g_other[nn][n][i]['t'] != missLat)
                    #ID stage
                    if g_other.nodes[nn]['evt'] == "upID" and g_other.nodes[n]['evt'] == "dwID": 
                        timing_indep = (g_other[nn][n][i]['t'] != 1)
                    #EX stage
                    if g_other.nodes[nn]['evt'] == "upEX" and g_other.nodes[n]['evt'] == "dwEX": 
                        timing_indep = (g_other[nn][n][i]['t'] != int(EXlat[g_other.nodes[nn]['instr']]))
                        #print("CHECK", g_other.nodes[nn]['evt'], g_other.nodes[n]['evt'], EXlat[g_other.nodes[nn]['instr']])
                    #MEM stage
                    if g_other.nodes[nn]['evt'] == "upMEM" and g_other.nodes[n]['evt'] == "dwMEM": 
                        #Check if instr is WB: MEMlat/missLat
                        if str(g_other.nodes[nn]['instr'])+"upWB" in g_other.nodes:
                            #PB2
                            #if MEMmiss[g_other.nodes[nn]['instr']] == '0': #ISSUE WITH LINE 290
                                #timing_indep = (g_other[nn][n][i]['t'] != int(MEMlat[g_other.nodes[nn]['instr']]))
                            if MEMmiss[g_other.nodes[nn]['instr']] == '1':
                                timing_indep = (g_other[nn][n][i]['t'] != missLat)
                        else: # case of ST
                            timing_indep = (g_other[nn][n][i]['t'] != 1)    
                    if g_other.nodes[nn]['evt'] == "upST" and g_other.nodes[n]['evt'] == "dwST": 
                        if MEMmiss[g_other.nodes[nn]['instr']] == '0':
                            timing_indep = (g_other[nn][n][i]['t'] != MEMlat[g_other.nodes[nn]['instr']])   
                        if MEMmiss[g_other.nodes[nn]['instr']] == '1':
                            timing_indep = (g_other[nn][n][i]['t'] != missLat)   
                """
                #Spot a variation iff 3 conditions: Dep +  
                if not timing_indep and res_usage and miss_info: 
                    variation, isTA = has_TA(path+[nn], acq, rel)
                else:
                    variation, isTA = False, False
                break_path.append(timing_indep or variation)
                if break_path[i]:
                    g_other[nn][n][i]['style'] = 'dashed'
                
            if all(break_path):
                print('Causality is broken: '+label(nn))
                print("PATH BREAK", get_paths.breakpoints)
                if nn not in get_paths.breakpoints: #+com_nodes
                    get_paths.breakpoints.append(nn)
                    get_paths([nn])
                else:
                    print('(Already explored)')
                    continue
            else:
                path.append(nn)
                get_paths(path)
                path.remove(nn)
    
    get_paths.breakpoints = list()

    
    if (str(N_instr-1)+'dwWB') in g_other.nodes :
        path = [str(N_instr-1)+'dwWB']
    if (str(N_instr-1)+'dwST') in g_other.nodes :
        path = [str(N_instr-1)+'dwST']
    get_paths(path)
        
    
if len(sys.argv) < 2:
    print('Wrong usage: $ python script.py input_file [output_path+prefix]')
    exit()

input_path = sys.argv[1]
filename = input_path.rsplit('.', 1)[0]
output_prefix = sys.argv[2] if len(sys.argv) == 3 else filename
with open(input_path) as input_file:
    file_content = input_file.read()
    graph1, graph2 = get_content(file_content)
    N_instr1, nodes1, edges1, IFmiss1, MEMmiss1, MEMlat, EXlat = parse_graph(graph1)
    N_instr2, nodes2, edges2, IFmiss2, MEMmiss2, MEMlat, EXlat = parse_graph(graph2)
    if N_instr1 != N_instr2:
        print('Warning: different program sizes!')
    N = min(N_instr1, N_instr2)
    g1 = build_graph(N, nodes1, edges1, IFmiss1, MEMmiss1, MEMlat, EXlat)
    g2 = build_graph(N, nodes2, edges2, IFmiss2, MEMmiss2, MEMlat, EXlat)
    
    print('Exec1')
    explore(g1, N, g2, IFmiss2, MEMmiss2, MEMlat, EXlat)
    print('\nExec2')
    explore(g2, N, g1, IFmiss1, MEMmiss1, MEMlat, EXlat)
    
    a1, a2 = nx.nx_agraph.to_agraph(g1), nx.nx_agraph.to_agraph(g2)
    a1.graph_attr.update(splines='ortho') #, overlap='false')
    a1.node_attr.update(width=1, fontsize=20.0, shape='box')
    a1.edge_attr.update(fontsize=20.0)
    a2.graph_attr.update(splines='ortho') #, overlap='false')
    a2.node_attr.update(width=1, fontsize=20.0, shape='box')
    a2.edge_attr.update(fontsize=20.0)
    '''for s in sub1:
        a1.add_subgraph(s, rank='same')
    for s in sub2:
        a2.add_subgraph(s, rank='same')'''
    
    a1.draw(output_prefix+'-AMP-ex1.png', prog='neato', format='png')
    a2.draw(output_prefix+'-AMP-ex2.png', prog='neato', format='png')
    
    '''f = open(output_prefix+'-edges.pkl', "w")
    pickle.dump(g1.edges.data(), f)
    pickle.dump(g2.edges.data(), f)
    f.close()'''
    
