import os

#N = number of generated models after pruning
N = 45
""" 
N instr/N models->N pruning models:
3/216->79 
4/4536->653
6/7185024->106109
"""

print("Empty?")

def CheckTA(word, file_path):
    result = {}
    with open(file_path, 'r') as file:
        content = file.read()
        if word in content:
            result = True
        else:
            result = False
    return result

if __name__ == "__main__":
    for i in range(1, N + 1):
        Model_name = f'M{i}'
        # Replace correct path + n with number of instructions in program
        folder_path = os.path.join("/home/lilia/THESIS/2PracticalWork/ProgGen/SIC/SIC_models_4Instr", Model_name)
        
        #CI TA check
        file1 = 'CI_output'
        file_path1 = os.path.join(folder_path, file1)
        if CheckTA('TA', file_path1):
            print("Model:" f'M{i}' "has a CI TA")
        if CheckTA('Standard', file_path1):
            print("Model:" f'M{i}' "has a STANDARD CI TA")

        #AMP TA check
        file2 = 'AMP_output'
        file_path2 = os.path.join(folder_path, file2)
        if CheckTA('TA', file_path2):
            print("Model:" f'M{i}' "has an AMP TA")
        if CheckTA('Standard', file_path2):
            print("Model:" f'M{i}' "has a STANDARD AMP TA")