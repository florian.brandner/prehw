# SCRIPT TO GENERATE MODELS BASED ON CONFIG -> USE TLC TO APPLY DETECTION PROCEDURES ON THEM -> CHECK IF TAs DETECTED OR NOT
#!/bin/bash

# Launch python3.8 virtual env 
#python3.8 -m venv env
#source env/bin/activate

pip install networkx
pip install pygraphviz

#ProgGen_script="ProgGenPruningSIC.py"
#python "$ProgGen_script"

tlc_command="tlc MC.tla >> MC_TE.out"

# Execute detection procedures 
#CI_script="/home/lilia/THESIS/2PracticalWork/ProgGen/SIC/SIC_CI_TA2.py"
#AMP_script="/home/lilia/THESIS/2PracticalWork/DetectionProcedures/SIC_AMP_TA.py"
    
#Num of generated models based on ProgLength: 3/80, 4/4536, 6/106110
#Replace n with instr/program 
N_instr=6
N_PrunedModels=106109
for ((i = 1; i < N_PrunedModels +1; i++)); do
    folder="/home/lilia/THESIS/2PracticalWork/ProgGen/SIC/SIC_models_${N_instr}Instr/M${i}"
    
    # Go to folder
    cd "$folder" 
    echo "$folder" 
    
    # Execute the TLC command
    tlc MC.tla >> MC_TE.out

    #python "$CI_script" MC_TE.out >> CI_output
    #python "$AMP_script" MC_TE.out >> AMP_output

    # Go back to the parent directory
    cd ..
done

#cd ..
#CheckTA_script="CheckTaSIC.py"
#python "$CheckTA_script"
